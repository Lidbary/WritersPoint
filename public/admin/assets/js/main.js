/*
    Created on : 1st January, 2016, 7:34:14 PM
    Author     : mike lidbary
    Email	   : lidbary@gmail.com
*/

// var host = '/'+window.location.pathname.split('/')[1]+'/public';
var host = '';

$('#product_id').change(function(){
var product_id=$('#product_id').val();
    $.get(host+'/orderdata',{product_id:product_id},function(data){
    	$('#baseprice').val(data);
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
    });
});
$('#urgency').change(function(){
var product_id=$('#product_id').val();
var urgencydata=$('#urgency').val();
    $.get(host+'/orderdata',{product_id:product_id,urgency:urgencydata},function(data){
    	$('#urgencyholder').val(data);
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
    });
});
$('#quality').change(function(){
var product_id=$('#product_id').val();
var qualitydata=$('#quality').val();
    $.get(host+'/orderdata',{product_id:product_id,quality:qualitydata},function(data){
    	$('#qualityholder').val(data);
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
    });
});
$('#spacing').change(function(){
var product_id=$('#product_id').val();
var spacingdata=$('#spacing').val();
    $.get(host+'/orderdata',{product_id:product_id,spacing:spacingdata},function(data){
    	$('#spacingholder').val(data);
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
    });
});
$('#academic_level').change(function(){
var product_id=$('#product_id').val();
var academic_leveldata=$('#academic_level').val();
    $.get(host+'/orderdata',{product_id:product_id,academic_level:academic_leveldata},function(data){
    	$('#levelholder').val(data);
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
    });
});
$('#pages').change(function(){
    	var baseprice=parseInt($('#baseprice').val());
		var urgency=parseInt($('#urgencyholder').val());
		var quality=parseInt($('#qualityholder').val());
		var spacing=parseInt($('#spacingholder').val());
		var level=parseInt($('#levelholder').val());
		var pages=parseInt($('#pages').val());
		var total=baseprice+urgency+quality+spacing+level;
		$('#total_p').val(total*pages);
		$('#total_price').text(total*pages);
		$('#per_page_price').text(baseprice+urgency+quality+spacing+level);
});

$('#calculate').click(function(event){
	event.preventDefault();
	var pages=parseInt($("#pages").val());
	var product_id=parseInt($("#product_id").val());
	var spacing=parseInt($("#spacing").val());
	// if(pages>0 && $product_id>0 && spacing>0){
	$.get(host+'/showpricing',{product_id:product_id,pages:pages,spacing:spacing},function(data){
		$("#pricing_content").html(data);
	});		
	// }

})