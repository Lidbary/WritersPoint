@extends('layouts.admin')

@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-bottom">Order</h3>

            <div class="md-card" style="background:#F0FFFF">
             @if(Session::has('success_message'))
             <div class="box-header with-border">
              <h3 class="box-title">Removable</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
                          <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                         @endif
              <div class="md-card-content large-padding">
                <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                <input type="hidden" name="cmd" value="_xclick">
                <input type="hidden" name="business" value="NV77KL97Q8BM6">
                <input type="hidden" name="lc" value="KE">
                <input type="hidden" name="item_name" value="pointwriters">
                <input type="hidden" name="item_number" value="123">
                <input type="hidden" name="custom" value="{{$order->id}}">
                <input type="hidden" name="amount" value="{{$order->client_price}}.00">
                <input type="hidden" name="currency_code" value="USD">
                <input type="hidden" name="button_subtype" value="services">
                <input type="hidden" name="no_note" value="0">
                <input type="hidden" name="cn" value="Add special instructions to the seller:">
                <input type="hidden" name="no_shipping" value="1">
                <input type="hidden" name="rm" value="1">
                <input type="hidden" name="return" value="http://ww.pointwriters.com/payments/return/{{$order->id}}">
                <input type="hidden" name="cancel_return" value="http://ww.pointwriters.com/payments/cancel/{{$order->id}}">
                <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>

                </div>
            </div>
        </div>
    </div>


@endsection
 @section('page-script')
   {!! Html::script('admin/assets/js/kendoui_custom.min.js') !!}
   {!! Html::script('admin/assets/js/pages/kendoui.min.js') !!}
    @stop
