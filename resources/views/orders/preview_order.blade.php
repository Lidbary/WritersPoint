@extends('layouts.landing')

@section('content')
       
<div class="main-wrapper">
    <div class="main">
        <div class="hero-content">
            <div class="container">
              <h2>Preview Order</h2>
              <!-- <form> -->
              {{ csrf_field() }}
              <div class="panel-group">
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong>Personal Details</strong></div>
                  <div class="panel-body">
                   @include('layouts.partial.content_header')
                  <div class="hero-content-carousel">
                    <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td>Firstname</td>
                          <td>{{$user->firstname}}</td>
                        </tr>
                        <tr>
                          <td>Lastname</td>
                          <td>{{$user->lastname}}</td>
                        </tr>
                        <tr>
                          <td>Username</td>
                          <td>{{$user->username}}</td>
                        </tr>
                        <tr>
                          <td>Phone Number</td>
                          <td>{{$user->mobile_number}}</td>
                        </tr>
                        <tr>
                          <td>Email</td>
                          <td>{{$user->email}}</td>
                        </tr>
                      </tbody>
                    </table>
                     </div>
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong>Order Details</strong></div>
                  <div class="panel-body">
                    <div class="hero-content-carousel">
                      <table class="table table-bordered">
                      <tbody>
                        <tr>
                          <td>Topic</td>
                          <td>{{$order->order_title}}</td>
                        </tr>
                        <tr>
                          <td>Work Type</td>
                          <td>{{$product->name}}</td>
                        </tr>
                        <tr>
                          <td>Urgency</td>
                          <td>{{$order->urgency}}</td>
                        </tr>
                        <tr>
                          <td>Quality</td>
                          <td>{{$order->quality}}</td>
                        </tr>
                        <tr>
                          <td>Spacing</td>
                          <td>{{$order->spacing}}</td>
                        </tr>                        
                        <tr>
                          <td>Pages</td>
                          <td>{{$order->no_of_pages}}</td>
                        </tr>
                        <tr>
                          <td>Subject Area</td>
                          <td>{{$order->subject_area}}</td>
                        </tr>
                        <tr>
                          <td>Academic Level</td>
                          <td>{{$order->order_level}}</td>
                        </tr>
                        <tr>
                          <td>Style</td>
                          <td>{{$order->style}}</td>
                        </tr>
                        <tr>
                          <td>No of Sources</td>
                          <td>{{$order->no_of_sources}}</td>
                        </tr>
                        <tr>
                          <td>Dictionary</td>
                          <td>{{$order->dictionary}}</td>
                        </tr>
                        <tr>
                          <td>Description</td>
                          <td>{{$order->description}}</td>
                        </tr>
                        <tr>
                          <td>File</td>
                          <td></td>
                        </tr>
                      </tbody>
                    </table>
                    </div>  
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong style="color: #000;">Invoice</strong></div>
                  <div class="panel-body">
                    <div class="total_price-float">
                        <div class="col3 cost-per-page" style="color: #000;">Cost Per Page: <span style="">$</span>
                        <div style="color: #000;" class="per_page_price" id="per_page_price">{{$order->client_price/$order->no_of_pages}}</div>
                        </div>
                        <div class="col3 discount-price" style="color: #000;">Discount: <span class="discount_per">
                        <div class="discount_percent" id="discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 discount-price" style="color: #000;">Ref: <span style="" class="ref_discount_per">
                        <div class="discount_percent" id="ref_discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 total-price" style="color: #000;">Total Price: $<span class="total">
                        <div class="total_price" id="total_price" style="color: #000;">{{$order->client_price}}</div>
                        <p>
                        </p><div class="clear clearfix"></div>
                        </span></div>
                        <p><span class="discount_percent" id="get_ref_amount" style=""></span><span class="discount_percent" id="get_real_dis_amount">0</span><span class="total_price1" id="total_price1" style="">0</span>
                        </p><div style="" id="getif_discount_value"></div>
                        <div class="clear clearfix"></div>
                    </div>
                    <input type="hidden" name="baseprice" id="baseprice" value="0">
                    <input type="hidden" name="urgencyholder" id="urgencyholder" value="0">
                    <input type="hidden" name="qualityholder" id="qualityholder" value="0">
                    <input type="hidden" name="spacingholder" id="spacingholder" value="0">
                    <input type="hidden" name="levelholder" id="levelholder" value="0">
                    <input type="hidden" name="total_p" id="total_p" value="0">
                    <!-- <button type="submit" class="btn btn-default">Proceed To Checkout</button> -->
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_xclick">
                    <input type="hidden" name="business" value="NV77KL97Q8BM6">
                    <input type="hidden" name="lc" value="KE">
                    <input type="hidden" name="item_name" value="pointwriters">
                    <input type="hidden" name="item_number" value="123">
                    <input type="hidden" name="custom" value="{{$order->id}}">
                    <input type="hidden" name="amount" value="{{$order->client_price}}.00">
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="hidden" name="button_subtype" value="services">
                    <input type="hidden" name="no_note" value="0">
                    <input type="hidden" name="cn" value="Add special instructions to the seller:">
                    <input type="hidden" name="no_shipping" value="1">
                    <input type="hidden" name="rm" value="1">
                    <input type="hidden" name="return" value="http://ww.pointwriters.com/payments/return/{{$order->id}}">
                    <input type="hidden" name="cancel_return" value="http://ww.pointwriters.com/payments/cancel/{{$order->id}}">
                    <input type="hidden" name="bn" value="PP-BuyNowBF:btn_paynowCC_LG.gif:NonHosted">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_paynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>
                  </div>
                </div>
              </div>
              <!-- </form> -->
            </div>
        </div><!-- /.hero-content -->
    </div>
</div>
@endsection