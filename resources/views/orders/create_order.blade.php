@extends('layouts.admin')

@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-bottom">Create New Order <a style="font-size:14px; margin-left:20px" href="{{URL::route('manage-orders')}}">View all Orders>>>></a></h3>

            <div class="md-card" style="background:#F0FFFF">
             @if(Session::has('success_message'))
             <div class="box-header with-border">
              <h3 class="box-title">Removable</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
              <!-- /.box-tools -->
            </div>
                          <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                         @endif
              <div class="md-card-content large-padding">
             <form id="form_validation" class="uk-form-stacked" method="POST" action="{{URL::route('create_order')}}" enctype="multipart/form-data">
              {{ csrf_field() }}
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="worktype">Work Type<span class="req">*</span></label>
                       <select id="product_id" name="product_id" class="md-input">
                                <option value="" disabled selected hidden>Select...</option>
                                <option value="">Select...</option>
                                <?php
                                foreach ($products as $value) {
                                    echo "<option value='".$value->id."'>".$value->name."</option>";
                                }
                                ?>
                            </select>
                          @if ($errors->has('doctype'))
                            <span class="help-block" style="color:#a94442">
                             <strong>{{ $errors->first('doctype') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">Title<span class="req">*</span></label>
                      <input type="text" id="topic" name="topic" value="{{ old('order_title') }}" data-parsley-trigger="change" class="md-input" />
                       @if ($errors->has('order_title'))
                            <span class="help-block" style="color:#a94442">
                             <strong>{{ $errors->first('order_title') }}</strong>
                            </span>
                        @endif
                    </div>
                  </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="academic_level">Level<span class="req">*</span></label>
                      <select id="academic_level" name="academic_level" class="md-input">
                       <option value="" disabled selected hidden>Select...</option>
                        <option value="1">High School</option>
                        <option value="2">College</option>
                        <option value="3">Undergraduate</option>
                        <option value="4">Master</option>
                        <option value="5">Ph.D</option>
                                
                            </select>
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Urgency</label>
                      <select id="urgency" name="urgency" class="md-input">
                                <option value="" disabled selected hidden>Select...</option>
                                <?php
                                foreach ($urgency as $value) {
                                    echo "<option value='".$value->id."'>".$value->name."</option>";
                                }
                                ?>
                                
                            </select>
                    </div>
                  </div>
                 
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="quality">Quality:<span class="req">*</span></label>
                      <select id="quality" name="quality" class="md-input">
                            <option value="" disabled selected hidden>Select...</option>
                            <option value="1">Standard Quality</option>
                            <option value="2">Premium Quality</option>
                            </select>
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="spacing">Spacing<span class="req">*</span></label>
                      <select id="spacing" name="spacing" class="md-input">
                                <option value="" disabled selected hidden>Select...</option>
                                <option  value="2">Double Spaced</option>
                                <option value="1">Single Spaced</option>
                            </select>
                    </div>
                  </div>
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Pages:<span class="req">*</span></label>
                       <select id="pages" name="pages" class="md-input">
                                <option value="" disabled selected hidden>Select...</option>
                           <?php
                                for ($i=1; $i < 200; $i++) { 
                                  if($i==1){
                                     echo "<option selected='selected' value='".$i."'>".$i."</option>";
                                   }else{
                                     echo "<option value='".$i."'>".$i."</option>";
                                   }
                                   
                                  }
                                ?>
                        </select>                    
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Subject or Discipline<span class="req">*</span></label>
                      <select id="subject_area" name="subject_area" class="md-input">
                                <option value="" disabled selected hidden>Select...</option>
                               <option value="English 101">English 101</option>
                               <option value="Composition">Composition</option>
          <option value="Accounting">Accounting</option>
          <option value="Accounting and Finance">Accounting and Finance </option> 
          <option value="Advertising">Advertising</option>
          <option value="Aeronautics">Aeronautics</option>
          <option value="African-American Studies">African-American Studies</option>
          <option value="Agricultural Studies">Agricultural Studies</option>
          <option value="Agriculture">Agriculture</option>
          <option value="Alternative Medicine">Alternative Medicine</option>
          <option value="American History">American History</option>
          <option value="American Literature">American Literature</option>
          <option value="Ancient History">Ancient History</option>
          <option value="Antarctic Studies">Antarctic Studies</option>
          <option value="Anthropology">Anthropology</option>
          <option value="Antique Literature">Antique Literature</option>
          <option value="Application Essay">Application Essay</option>
          <option value="Applied Psychology">Applied Psychology</option>
          <option value="Archeology">Archeology</option>
          <option value="Architecture">Architecture</option>
          <option value="Art">Art</option>
          <option value="Art Curatorship">Art Curatorship</option>
          <option value="Art History">Art History</option>
          <option value="Art History and Theory">Art History and Theory</option>
          <option value="Art Theory">Art Theory</option>
          <option value="Asian Literature">Asian Literature</option>
          <option value="Asian Studies">Asian Studies</option>
          <option value="Astronomy">Astronomy</option>
          <option value="Audiology">Audiology</option>
          <option value="Aviation">Aviation</option>
              <option value="Biochemistry">Biochemistry</option>
              <option value="Bioengineering">Bioengineering</option>
              <option value="Biological Sciences">Biological Sciences</option>
              <option value="Biology">Biology</option>
              <option value="Biosecurity">Biosecurity</option>
              <option value="Biotechnology">Biotechnology</option>
              <option value="Botany">Botany</option>
              <option value="Business">Business</option>
              <option value="Business Administration">Business Administration</option>
              <option value="Business Economics">Business Economics</option>
              <option value="Business Management">Business Management</option>
              <option value="Business studies">Business studies</option>

              <option value="Canadian Studies">Canadian Studies</option>
              <option value="Case Study">Case Study</option>
              <option value="Cellular and Molecular Biology">Cellular and Molecular Biology</option>
              <option value="Chemical and Process Engineering">Chemical and Process Engineering</option>
              <option value="Chemistry">Chemistry</option>
              <option value="Child and Family Psychology">Child and Family Psychology</option>
              <option value="Chinese">Chinese</option>
              <option value="Cinema Studies">Cinema Studies</option>
              <option value="Civil Engineering">Civil Engineering</option>
              <option value="Classical Studies">Classical Studies</option>
              <option value="Classics">Classics</option>
              <option value="Clinical Psychology">Clinical Psychology</option>
              <option value="Clinical Teaching">Clinical Teaching</option>
              <option value="Communication">Communication</option>
              <option value="Communication Disorders">Communication Disorders</option>
              <option value="Communication Strategies">Communication Strategies</option>
              <option value="Communications and Media">Communications and Media</option>
              <option value="Company Analysis">Company Analysis</option>
              <option value="Computational and Applied Mathematics">Computational and Applied Mathematics</option>
              <option value="Computer Engineering">Computer Engineering</option>
              <option value="Computer Science">Computer Science</option>
              <option value="Computer-Assisted Language Learning">Computer-Assisted Language Learning</option>
              <option value="Construction Management">Construction Management</option>
              <option value="Counselling">Counselling</option>
              <option value="Creative writing">Creative writing</option>
              <option value="Criminal Justice">Criminal Justice</option>
              <option value="Criminology">Criminology</option>
              <option value="Cultural Studies">Cultural Studies</option>

              <option value="Dance">Dance</option>
              <option value="Design Analysis">Design Analysis</option>
              <option value="Development Studies">Development Studies</option>
              <option value="Digital Humanities">Digital Humanities</option>
              <option value="Diplomacy and International Relations">Diplomacy and International Relations</option>
              <option value="Drama">Drama</option>

              <option value="Early Childhood Teacher Education">Early Childhood Teacher Education</option>
              <option value="Earth and space sciences">Earth and space sciences</option>
              <option value="Earthquake Engineering">Earthquake Engineering</option>
              <option value="East European Studies">East European Studies</option>
              <option value="Ecology">Ecology</option>
              <option value="E-Commerce">E-Commerce</option>
              <option value="Economics">Economics</option>
              <option value="Education">Education</option>
              <option value="Education Theories">Education Theories</option>
              <option value="e-Learning and Digital Technologies in Education">e-Learning and Digital Technologies in Education</option><option value="Electrical and Electronic Engineering">Electrical and Electronic Engineering</option>
              <option value="Engineering">Engineering</option>
              <option value="Engineering Geology">Engineering Geology</option><option value="Engineering Management">Engineering Management</option>
              <option value="Engineering Mathematics">Engineering Mathematics</option>
              <option value="English Literature">English Literature</option>
              <option value="Environmental Issues">Environmental Issues</option>
              <option value="Environmental Science">Environmental Science</option>
              <option value="Ethics">Ethics</option>
              <option value="European and European Union Studies">European and European Union Studies</option>
              <option value="European Union Studies">European Union Studies</option>
              <option value="Evolutionary Biology">Evolutionary Biology</option>


              <option value="Film & Theater studies">Film & Theater studies</option>
              <option value="Finance">Finance</option>
              <option value="Finance and Accounting">Finance and Accounting</option>
              <option value="Financial Engineering">Financial Engineering</option>
              <option value="Fine Arts">Fine Arts</option>
              <option value="Fire Engineering">Fire Engineering</option>
              <option value="Forest Engineering">Forest Engineering</option>
              <option value="Forestry">Forestry</option>
              <option value="French">French</option>
              <option value="Freshwater Management">Freshwater Management</option>

              <option value="Gender and sexual studies">Gender and sexual studies</option>
              <option value="Geographic Information Science">Geographic Information Science</option>
              <option value="Geography">Geography</option>
              <option value="Geology">Geology</option>
              <option value="German">German</option>
              <option value="Graphic Design">Graphic Design</option>

              <option value="Hazard and Disaster Management">Hazard and Disaster Management</option>
              <option value="Health Sciences">Health Sciences</option>
              <option value="Higher Education">Higher Education</option>
              <option value="History">History</option>
              <option value="Hōaka Pounamu: Te Reo Māori Bilingual and Immersion Teaching">Hōaka Pounamu: Te Reo Māori Bilingual and Immersion Teaching</option>
              <option value="Holocaust">Holocaust</option>
              <option value="Human Interface Technology">Human Interface Technology</option>
              <option value="Human Resource Management">Human Resource Management</option>
              <option value="Human Services">Human Services</option>

              <option value="Inclusive and Special Education">Inclusive and Special Education</option>
              <option value="Industrial and Organisational Psychology">Industrial and Organisational Psychology</option>
              <option value="Information Systems">Information Systems</option>
              <option value="Information technology">Information technology</option>
              <option value="International Business">International Business</option>
              <option value="International Law and Politics">International Law and Politics</option>
              <option value="International Business/Trade">International Business/Trade</option>
              <option value="Internet">Internet</option>
              <option value="Investment">Investment</option>
              <option value="IT Management">IT Management</option>

              <option value="Japanese">Japanese</option>
              <option value="Journalism">Journalism</option>
              <option value="Journalism, mass media and communication">Journalism, mass media and communication</option>

              <option value="Latin-American Studies">Latin-American Studies</option>
              <option value="Law">Law</option>
              <option value="Leadership">Leadership</option>
              <option value="Learning Support">Learning Support</option>
              <option value="Legal Issues">Legal Issues</option><option value="Linguistics">Linguistics</option><option value="Literacy">Literacy</option><option value="Literature">Literature</option>
              <option value="Logistics">Logistics</option>
              <option value="Logic and programming">Logic and programming</option>

              <option value="Management">Management</option>
              <option value="Management Science">Management Science</option>
              <option value="Māori">Māori</option>
              <option value="Māori and Indigenous Studies">Māori and Indigenous Studies</option>
              <option value="Marketing">Marketing</option>
              <option value="Mathematical Physics">Mathematical Physics</option>
              <option value="Mathematics">Mathematics</option>
              <option value="Mathematics and Philosophy">Mathematics and Philosophy</option>
              <option value="Mechanical Engineering">Mechanical Engineering</option><option value="Mechatronics Engineering">Mechatronics Engineering</option>
              <option value="Media and Communication">Media and Communication</option>
              <option value="Medical Physics">Medical Physics</option>
              <option value="Medicine">Medicine</option>
              <option value="Medicine and Health">Medicine and Health</option>
              <option value="Microbiology">Microbiology</option>
              <option value="Military sciences">Military sciences</option>
              <option value="Movies">Movies</option>
              <option value="Music">Music</option>

              <option value="Native-American Studies">Native-American Studies</option>
              <option value="Natural Resources Engineering">Natural Resources Engineering</option>
              <option value="Nature">Nature</option>
              <option value="Nursing">Nursing</option>
              <option value="Nutrition">Nutrition</option>

              <option value="Operations and Supply Chain Management">Operations and Supply Chain Management</option>
              <option value="Other">Other</option>

              <option value="Pacific Studies">Pacific Studies</option>
              <option value="Painting">Painting</option>
              <option value="Palliative Care">Palliative Care</option>
              <option value="Pedagogy">Pedagogy</option>
              <option value="Pharmacology">Pharmacology</option>
              <option value="Philosophy">Philosophy</option>
              <option value="Photography">Photography</option>
              <option value="Physical Education">Physical Education</option>
              <option value="Physics">Physics</option>
              option value="Plant Biology">Plant Biology</option>
              <option value="Political Science">Political Science</option>
              <option value="Primary Teacher Education">Primary Teacher Education</option>
              <option value="Printmaking">Printmaking</option>
              <option value="Professional Development">Professional Development</option>
              <option value="Psychology">Psychology</option>
              <option value="Public Health">Public Health</option>
              <option value="Public Relations">Public Relations</option>
              <option value="Public Safety">Public Safety</option>

              <option value="Religion and Theology">Religion and Theology</option>
              <option value="Resilience and Sustainability">Resilience and Sustainability</option>
              <option value="Russian">Russian</option>

              <option value="Science and Entrepreneurship">Science and Entrepreneurship</option>
              <option value="Science Education">Science Education</option>
              <option value="Science, Māori and Indigenous Knowledge
              Sculpture">Science, Māori and Indigenous Knowledge
              Sculpture</option>
              <option value="Seafood Sector: Management and Science">Seafood Sector: Management and Science</option>
              <option value="Secondary Teacher Education">Secondary Teacher Education</option>
              <option value="Software Engineering">Software Engineering</option>
              <option value="Shakespeare Studies">Shakespeare Studies</option>
              <option value="Social Work">Social Work</option>
              <option value="Sociology">Sociology</option>
              <option value="Soil Science">Soil Science</option>
              <option value="South Asia Studies">South Asia Studies</option>
              <option value="Spanish">Spanish</option>
              <option value="Specialist Teaching">Specialist Teaching</option>
              <option value="Speech and Language Pathology, Speech and Language Sciences">Speech and Language Pathology, Speech and Language Sciences</option>
              <option value="Sport Coaching">Sport Coaching</option>
              <option value="Statistics">Statistics</option>
              <option value="Strategic Leadership">Strategic Leadership</option>
              <option value="Strategy and Entrepreneurship">Strategy and Entrepreneurship</option>

              <option value="Taxation and Accounting">Taxation and Accounting</option>
              <option value="Te Reo Māori">Te Reo Māori</option>
              <option value="Teacher Education">Teacher Education</option>
              <option value="Teacher's Career">Teacher's Career</option>
              <option value="Teaching and Learning">Teaching and Learning</option>
              <option value="Technology">Technology</option>
              <option value="Tertiary Teaching">Tertiary Teaching</option>
              <option value="Theatre">Theatre</option>
              <option value="Tourism">Tourism</option>
              <option value="Trade">Trade</option>
              <option value="Transportation Engineering">Transportation Engineering</option>

              <option value="Water Resource Management">Water Resource Management</option>
              <option value="Web Design">Web Design</option>
              <option value="West European Studies">West European Studies</option>
                                              
                                              
              </select>
                    </div>
                  </div>
                   
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="no_of_pages">Style:<span class="req">*</span></label>
                      <select id="style" name="style" class="md-input">
                            <option value="" disabled selected hidden>Select...</option>
                              <option value="">Please Select Style</option><option value="1">APA</option><option value="2">MLA</option><option value="3">Turabian</option><option value="4">Chicago</option><option value="5">Harvard</option><option value="6">Oxford</option><option value="7">Vancouver</option><option value="8">CBE</option><option value="9">Other</option>
                            </select>
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Dictionary<span class="req">*</span></label>
                      <select id="dictionary" name="dictionary" class="md-input">
                              <option value="" disabled selected hidden>Select...</option>
                                <option value="uk">U.K.</option><option value="us">U.S.</option>
                            </select>
                    </div>
                  </div>
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Number of sources:<span class="req">*</span></label>
                      <select id="source" name="source" class="md-input">
                      <option value="" disabled selected hidden>Select...</option>
                             <?php
                                for ($i=1; $i < 200; $i++) { 
                                  if($i==1){
                                     echo "<option selected='selected' value='".$i."'>".$i."</option>";
                                   }else{
                                     echo "<option value='".$i."'>".$i."</option>";
                                   }
                                   
                                  }
                              ?>       
                        </select>
                         @if ($errors->has('no_of_sources'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('no_of_sources') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Order instructions:<span class="req">*</span></label>
                      <textarea  name="description" class="md-input"></textarea>
                       @if ($errors->has('description'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('description') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>
                   
                </div>
                <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 1:</label>
                        <div class="col-sm-10">
                        <input name="attachment1" size="35" type="file"></div>
                        </div>
                               <div class="panel panel-primary">
                  <div class="panel-heading"><strong style="color: #000;">Invoice</strong></div>
                  <div class="panel-body">
                    <div class="total_price-float">
                        <div class="col3 cost-per-page" style="color: #000;">Cost Per Page: <span style="">$</span>
                        <div style="color: #000;" class="per_page_price" id="per_page_price">0.00</div>
                        </div>
                        <div class="col3 discount-price" style="color: #000;">Discount: <span class="discount_per">
                        <div class="discount_percent" id="discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 discount-price" style="color: #000;">Ref: <span style="" class="ref_discount_per">
                        <div class="discount_percent" id="ref_discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 total-price" style="color: #000;">Total Price: $<span class="total">
                        <div class="total_price" id="total_price" style="color: #000;">0.00</div>
                        <p>
                        </p><div class="clear clearfix"></div>
                        </span></div>
                        <div class="clear clearfix"></div>
                    </div>
                    <input type="hidden" name="baseprice" id="baseprice" value="0">
                    <input type="hidden" name="urgencyholder" id="urgencyholder" value="0">
                    <input type="hidden" name="qualityholder" id="qualityholder" value="0">
                    <input type="hidden" name="spacingholder" id="spacingholder" value="0">
                    <input type="hidden" name="levelholder" id="levelholder" value="0">
                    <input type="hidden" name="total_p" id="total_p" value="0">
                  </div>
                </div>
                <div class="uk-grid">
                  <div class="uk-width-1-1">
                    <button type="submit" class="md-btn md-btn-primary">Place Order</button>
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
    </div>


@endsection
 @section('page-script')
   {!! Html::script('admin/assets/js/kendoui_custom.min.js') !!}
   {!! Html::script('admin/assets/js/pages/kendoui.min.js') !!}
    @stop
