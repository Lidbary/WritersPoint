@extends('layouts.admin')

@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-bottom">Create New Work Type <a style="font-size:14px; margin-left:20px" href="{{URL::route('products')}}">View all Work Types >>>></a></h3>

            <div class="md-card" style="background:#F0FFFF">
             @if(Session::has('success_message'))
                          <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                         @endif
              <div class="md-card-content large-padding">
             <form id="form_validation" class="uk-form-stacked" method="POST" action="{{URL::route('new_product')}}">
              {{ csrf_field() }}
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">Name<span class="req">*</span></label>
                      <input type="text" name="name" value="" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">
                      Base Price<span class="req">*</span></label>
                      <input type="text" name="baseprice" value="" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
               <div class="uk-grid" data-uk-grid-margin>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Description:<span class="req">*</span></label>
                      <textarea  name="description" class="md-input"></textarea>
                    </div>
                  </div>
                </div> 
                <h4>Level (indicate Amount added on top of the base price)</h4>
                <hr>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">High School<span class="req">*</span></label>
                      <input type="text" name="level_1" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">Colledge<span class="req">*</span></label>
                      <input type="text" name="level_2" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">Undergraduate<span class="req">*</span></label>
                      <input type="text" name="level_3" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">
                      Masters<span class="req">*</span></label>
                      <input type="text" name="level_4" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">Ph.D<span class="req">*</span></label>
                      <input type="text" name="level_5" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <h4>Quality (indicate Amount added on top of the base price)</h4>
                <hr>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">Standard Quality<span class="req">*</span></label>
                      <input type="text" name="quality_1" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">Premium Quality<span class="req">*</span></label>
                      <input type="text" name="quality_2" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <h4>Spacing (indicate Amount added on top of the base price)</h4>
                <hr>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email">Single Spacing<span class="req">*</span></label>
                      <input type="text" name="spacing_1" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="email">Double Spacing<span class="req">*</span></label>
                      <input type="text" name="spacing_2" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <h4>Urgency (indicate Amount added on top of the base price)</h4>
                <hr>
                <?php
                foreach ($urgency as $value) {
                ?>
                <div class="uk-grid" data-uk-grid-margin>
                  <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                    <label for="email"><?=$value->name?><span class="req">*</span></label>
                      <input type="text" name="urgency_<?=$value->id?>" value="0" data-parsley-trigger="change" class="md-input" />
                    </div>
                  </div>
                </div>
                <?php
                }
                ?>
            
                <div class="uk-grid">
                  <div class="uk-width-1-1">
                    <button type="submit" class="md-btn md-btn-primary">Create Work Type</button>
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
    </div>


@endsection
 @section('page-script')
   {!! Html::script('admin/assets/js/kendoui_custom.min.js') !!}
   {!! Html::script('admin/assets/js/pages/kendoui.min.js') !!}
    @stop
