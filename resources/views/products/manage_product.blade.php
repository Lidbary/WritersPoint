@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_b uk-margin-bottom">Manage Work Types <a style="font-size:14px; margin-left:20px" href="{{URL::route('new_product')}}">create new work type>>>></a></h3>

            <div class="md-card-content">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>

                    <div class="uk-width-medium-1-12">
                        
                        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#switcher-content-a-fade', animation: 'fade'}">
                     <!--        <li><a class= uk-active" href="#">All</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Create New Product</a></li> -->
<!--                             <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Pending</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Confirmed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Unconfirmed</a></li>
                            <li><a class="md-btn md-btn-warning md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Revision</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Editing</a></li>
                            <li><a class="md-btn md-btn-success md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Completed</a></li>
                            <li><a class="md-btn md-btn-primary md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Approved</a></li>
                            <li><a class="md-btn md-btn-danger md-btn-mini md-btn-wave-light waves-effect waves-button waves-light" href="#">Rejected</a></li> -->
                          
                        </ul>
                        <ul id="switcher-content-a-fade" class="uk-switcher uk-margin">
                            <li>
                              <h5 class="heading_c uk-margin-bottom">All Work Types</h5>
                                <div class="md-card uk-margin-medium-bottom">
                                    <div class="md-card-content">
                                        <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                                            <thead>
                                            <tr style="background:#eeeeee">
                                           
                                                <th>Work Type No</th>
                                                <th>Name</span></th>
                                                <th>Description</th>
                                                <th>Base Price</th>
                                                <th>Created_at</th>
                                                <th>Updated_at</th>  
                                                

                                            </tr>
                                            </thead>

                                            <tfoot>
                                            <tr style="background:#eeeeee">
                                               
                                                <th>Work Type No</th>
                                                <th>Name</span></th>
                                                <th>Description</th>
                                                <th>Base Price</th>
                                                <th>Created_at</th>
                                                <th>Updated_at</th>   
                                            </tr>
                                            </tfoot>

                                            <tbody>
                                            @foreach ($products as $product)
                                            <tr>
                                              
                                                <td><a href="{{ URL::route('viewProduct', $product->id) }}">{{$product->id}}</a></td>
                                                <td>{{$product->name}}</td>
                                                <td><span style="color:#2196f3">{{$product->description}}</span></td>
                                                 <td>{{$product->price}}</td>
                                                <td><span style="color:#2196f3">{{$product->created_at}}</span></td>
                                                 <td><span style="color:#2196f3">{{$product->updated_at}}</span></td>                                   
                                            </tr>
                                            @endforeach
                                             

                                             

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>


@endsection
 @section('page-script')
    {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
