@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card-content">
                <div class="uk-grid uk-grid-divider" data-uk-grid-margin>

                    <div class="uk-width-medium-1-8">
                        
                        <ul class="uk-subnav uk-subnav-pill" data-uk-switcher="{connect:'#switcher-content-a-fade', animation: 'fade'}">
                          
                        </ul>
                        <ul id="switcher-content-a-fade" class="uk-switcher uk-margin">
                            <li>
                              <h5 class="heading_c uk-margin-bottom">{{$product->name}}</h5>
                                <div class="md-card uk-margin-medium-bottom">
                                    <div class="md-card-content">
                                        <table id="dt_colVis" class="uk-table" cellspacing="0" width="100%">
                                        <tr>
                                            <th>Name</th>
                                            <td>{{$product->name}}</td>
                                        </tr>                                        
                                        <tr>
                                            <th>Description</th>
                                            <td>{{$product->decription}}</td>
                                        </tr>                                        
                                        <tr>
                                            <th>Base Price</th>
                                            <td>{{$product->price}}</td>
                                        </tr>                                        
                                        </table>
                                    </div>
                                </div>
                            </li>
                            
                        </ul>
                    </div>
                  </div>
                </div>
              </div>
        </div>
    </div>


@endsection
 @section('page-script')
    {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
