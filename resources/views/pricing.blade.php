@extends('layouts.landing')

@section('content')
       
<div class="main-wrapper">
    <div class="main">
        <div class="hero-content">
            <div class="container">
                <div class="row">

                    <div class="col-sm-9 col-md-8 col-md-offset-1">
                        <div class="hero-content-carousel">
                            <h2>Our Affordable Price Plan Structure</h2>
                     <form role="form" method="POST" action="{{ url('/login') }}">
                     {{ csrf_field() }}
                        <div class="form-group">
                            <label for="product_id">Select Work Type</label>
                            <select id="product_id" name="product_id" class="form-control">
                            <option value="">-- Please select document --</option>
                            	<?php
                            	foreach ($products as $value) {
                            		echo "<option value='".$value->id."'>".$value->name."</option>";
                            	}
                            	?>
                        	</select>
                        </div>
                        <div class="form-group">
                            <label for="pages">Select Pages</label>
                            <select id="pages" name="pages" class="form-control">
                            	<?php
                            	for ($i=1; $i < 200; $i++) { 
                            		echo "<option value='".$i."'>".$i."</option>";
                            	}
                            	?>
                        	</select>
                        </div>
                        <div class="form-group">
                            <label for="pages">Select Spacing</label>
                            <select id="spacing" name="spacing" class="form-control">
                            	<option value="2">Double Spacing (275 Words Per Page)</option>
                            	<option value="1">Single Spacing (550 Words Per Page)</option>
                        	</select>
                        </div>
                        <div class="form-group">
                            <button type="submit" id="calculate" class="btn btn-primary btn-block">Calculate Price</button>
                        </div><!-- /.form-group
<!-- 
                        <hr> -->

                    
                    </form>
            
                    <div id="pricing_content" class="container">
                    </div>
                            
                        </div><!-- /.hero-content-content -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.hero-content -->
    </div>
</div>
@endsection