@extends('layouts.landing')

@section('content')
       
<div class="main-wrapper">
    <div class="main">
        <div class="hero-content">
            <div class="container">
              <h2>Make Order</h2>
              <form role="form" method="POST" action="{{ url('/order') }}" class="form-horizontal" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="panel-group">
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong>Personal Details</strong></div>
                  <div class="panel-body">
                  <div class="hero-content-carousel">
                    
                      <div class="form-group {{ $errors->has('firstname') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="name">Firstname:*</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="firstname" id="firstname" placeholder="Enter first name">
                        </div>
                      </div>                       
                      <div class="form-group {{ $errors->has('lastname') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="name">Lastname:*</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="lastname" id="lastname" placeholder="Enter last name">
                        </div>
                      </div>                       
                      <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="name">Username:*</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="username" id="username" >
                        </div>
                      </div>                      
                      <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="name">Email Address:*</label>
                        <div class="col-sm-10">
                          <input type="email" class="form-control" name="email" id="email" placeholder="Enter email">
                        </div>
                      </div>
                      <div class="form-group {{ $errors->has('pwd') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="pwd">Password:*</label>
                        <div class="col-sm-10">
                          <input type="password" class="form-control" name="pwd" id="pwd" placeholder="Enter password">
                        </div>
                      </div>
                      <div class="form-group {{ $errors->has('country') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="country">Country:*</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="country" name="country">
                          <?php
                          foreach ($countries as $value) {
                              echo "<option value='".$value->id."'>".$value->nicename."</option>";
                          }
                          ?>
                          </select>
                        </div>
                      </div>
                     <div class="form-group {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="phone_number">Contact Number:*</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="phone_number" id="phone_number" placeholder="Enter full phone_number">
                        </div>
                      </div>  
                     </div>
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong>Order Details</strong></div>
                  <div class="panel-body">
                    <div class="hero-content-carousel">
                      <div class="form-group {{ $errors->has('topic') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="topic">Topic:*</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="topic" id="topic" placeholder="">
                        </div>
                      </div>
                    <div class="form-group {{ $errors->has('product_id') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="product_id">Type of Document:*</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="product_id" name="product_id">
                          <option value="">-- Please select document --</option>
                                <?php
                                foreach ($products as $value) {
                                    echo "<option value='".$value->id."'>".$value->name."</option>";
                                }
                                ?>
                          </select>
                        </div>
                      </div>
                    <div class="form-group {{ $errors->has('urgency') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="urgency">Urgency:*</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="urgency" name="urgency">
                            <option value="">Select</option>
                                <?php
                                foreach ($urgency as $value) {
                                    echo "<option value='".$value->id."'>".$value->name."</option>";
                                }
                                ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('quality') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="quality">Quality:*</label>
                        <div class="col-sm-10">
                          <select class="form-control" id="quality" name="quality">
                            <option value="">Select Quality</option>
                            <option value="1">Standard Quality</option>
                            <option value="2">Premium Quality</option>
                          </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('spacing') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="spacing">Select Spacing</label>
                        <div class="col-sm-10">
                            <select id="spacing" name="spacing" class="form-control">
                                <option value="">Select Spacing</option>
                                <option value="1">Single Spacing (550 Words Per Page)</option>
                                <option value="2">Double Spacing (275 Words Per Page)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('pages') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="pages">Select Pages</label>
                        <div class="col-sm-10">
                            <select id="pages" name="pages" class="form-control">
                                <?php
                                for ($i=1; $i < 200; $i++) { 
                                  if($i==1){
                                     echo "<option selected='selected' value='".$i."'>".$i."</option>";
                                   }else{
                                     echo "<option value='".$i."'>".$i."</option>";
                                   }
                                   
                                  }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('academic_level') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="academic_level">Please Select Academic Level:*</label>
                        <div class="col-sm-10">
                            <select id="academic_level" name="academic_level" class="form-control">
                                <option value="">Select Academic Level</option>
                                <option value="1">High School</option>
                                <option value="2">Colledge</option>
                                <option value="3">Undergraduate</option>
                                <option value="4">Master</option>
                                <option value="5">Ph.D</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('subject_area') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="subject_area">Subject Area;*</label>
                        <div class="col-sm-10">
                            <select id="subject_area" name="subject_area" class="form-control">
                                <option value="">Please Select Subject Area</option><option value="10"> Art</option><option value="11">&nbsp;&nbsp;&nbsp; Architecture</option><option value="12">&nbsp;&nbsp;&nbsp; Dance</option><option value="13">&nbsp;&nbsp;&nbsp; Design Analysis</option><option value="14">&nbsp;&nbsp;&nbsp; Drama</option><option value="15">&nbsp;&nbsp;&nbsp; Movies</option><option value="16">&nbsp;&nbsp;&nbsp; Music</option><option value="17">&nbsp;&nbsp;&nbsp; Paintings</option><option value="18">&nbsp;&nbsp;&nbsp; Theatre</option><option value="19"> Biology</option><option value="20"> Business</option><option value="21"> Chemistry</option><option value="22"> Communication and Media</option><option value="23">&nbsp;&nbsp;&nbsp; Advertising</option><option value="24">&nbsp;&nbsp;&nbsp; Communication Strategies</option><option value="25">&nbsp;&nbsp;&nbsp; Journalism</option><option value="26">&nbsp;&nbsp;&nbsp; Public Relations</option><option value="27"> Creative Writing</option><option value="28"> Economics</option><option value="29">&nbsp;&nbsp;&nbsp; Accounting</option><option value="30">&nbsp;&nbsp;&nbsp; Case Study</option><option value="31">&nbsp;&nbsp;&nbsp; Company Analysis</option><option value="32">&nbsp;&nbsp;&nbsp; E-Commerce</option><option value="33">&nbsp;&nbsp;&nbsp; Finance</option><option value="34">&nbsp;&nbsp;&nbsp; Investment</option><option value="35">&nbsp;&nbsp;&nbsp; Logistics</option><option value="36">&nbsp;&nbsp;&nbsp; Trade</option><option value="39"> Education</option><option value="40">&nbsp;&nbsp;&nbsp; Application Essay</option><option value="41">&nbsp;&nbsp;&nbsp; Education Theories</option><option value="42">&nbsp;&nbsp;&nbsp; Pedogogy</option><option value="43">&nbsp;&nbsp;&nbsp; Teacher's Career</option><option value="44"> Engineering</option><option value="46"> English</option><option value="47"> Ethics</option><option value="48"> History</option><option value="49">&nbsp;&nbsp;&nbsp; African-American Studies</option><option value="50">&nbsp;&nbsp;&nbsp; American History</option><option value="51">&nbsp;&nbsp;&nbsp; Asian Studis</option><option value="52">&nbsp;&nbsp;&nbsp; Canadian Studies</option><option value="53">&nbsp;&nbsp;&nbsp; East European Studies</option><option value="54">&nbsp;&nbsp;&nbsp; Holocaust</option><option value="55">&nbsp;&nbsp;&nbsp; Latin-American Studies</option><option value="56">&nbsp;&nbsp;&nbsp; Native-American Studies</option><option value="57">&nbsp;&nbsp;&nbsp; West European Studies</option><option value="58"> Law</option><option value="59">&nbsp;&nbsp;&nbsp; Criminology</option><option value="60">&nbsp;&nbsp;&nbsp; Legal Issues</option><option value="61"> Linguistics</option><option value="62"> Literature</option><option value="63">&nbsp;&nbsp;&nbsp; American Literature</option><option value="64">&nbsp;&nbsp;&nbsp; Antique Literature</option><option value="65">&nbsp;&nbsp;&nbsp; Asian Literature</option><option value="66">&nbsp;&nbsp;&nbsp; English Literature</option><option value="67">&nbsp;&nbsp;&nbsp; Shakespeare Studies</option><option value="68"> Management</option><option value="69"> Marketing</option><option value="70"> Mathematics</option><option value="71"> Medicine and Health</option><option value="72">&nbsp;&nbsp;&nbsp; Alternative Medicine</option><option value="73">&nbsp;&nbsp;&nbsp; Healthcare</option><option value="74">&nbsp;&nbsp;&nbsp; Nursing</option><option value="75">&nbsp;&nbsp;&nbsp; Nurtition</option><option value="76">&nbsp;&nbsp;&nbsp; Pharmacology</option><option value="77">&nbsp;&nbsp;&nbsp; Sport</option><option value="78"> Nature</option><option value="79">&nbsp;&nbsp;&nbsp; Agricultural Studies</option><option value="80">&nbsp;&nbsp;&nbsp; Anthropology</option><option value="81">&nbsp;&nbsp;&nbsp; Astronomy</option><option value="82">&nbsp;&nbsp;&nbsp; Environmental Issues</option><option value="83">&nbsp;&nbsp;&nbsp; Geography</option><option value="84">&nbsp;&nbsp;&nbsp; Geology</option><option value="85"> Philosophy</option><option value="86"> Physics</option><option value="87"> Political Science</option><option value="88"> Psychology</option><option value="89"> Religion and Theology</option><option value="90"> Sociology</option><option value="91"> Technology</option><option value="92">&nbsp;&nbsp;&nbsp; Aeronautics</option><option value="93">&nbsp;&nbsp;&nbsp; Aviation</option><option value="94">&nbsp;&nbsp;&nbsp; Computer Science</option><option value="95">&nbsp;&nbsp;&nbsp; Internet</option><option value="96">&nbsp;&nbsp;&nbsp; IT Management</option><option value="97">&nbsp;&nbsp;&nbsp; Web Design</option><option value="98"> Toursim</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('style') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="style">Style:*</label>
                        <div class="col-sm-10">
                            <select id="style" name="style" class="form-control">
                                <option value="">Please Select Style</option><option value="1">APA</option><option value="2">MLA</option><option value="3">Turabian</option><option value="4">Chicago</option><option value="5">Harvard</option><option value="6">Oxford</option><option value="7">Vancouver</option><option value="8">CBE</option><option value="9">Other</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('source') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="source">Please Select Number Of Sources</label>
                        <div class="col-sm-10">
                            <select id="source" name="source" class="form-control">
                                <?php
                                for ($i=1; $i < 200; $i++) { 
                                    echo "<option value='".$i."'>".$i."</option>";
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('dictionary') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="dictionary">Please Select Dictionary:*</label>
                        <div class="col-sm-10">
                            <select id="dictionary" name="dictionary" class="form-control">
                                <option value="">Please Select Dictionary</option><option value="uk">U.K.</option><option value="us">U.S.</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('description') ? ' has-error' : '' }}">
                        <label style="color: #000;" class="control-label col-sm-2" for="description">Order Description:*</label>
                        <div class="col-sm-10">
                            <textarea cols="4" rows="4" id="description" name="description" class="form-control"></textarea> >
                        </div>
                    </div>
                    <span class="upload-files">
                        <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 1:</label>
                        <div class="col-sm-10">
                        <input name="attachment1" size="35" type="file"></div>
                        </div>
<!--                         <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 2:</label>
                        <div class="col-sm-10">
                        <input name="attachment2" size="35" type="file"></div>
                        </div>
                        <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 3:</label>
                        <div class="col-sm-10">
                        <input name="attachment3" size="35" type="file"></div>
                        </div> -->
  <!--                       <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 4:</label>
                        <div class="col-sm-10">
                        <input name="attachment4" size="35" type="file"></div>
                        </div>
                        <div class="upload-fields form-group">
                        <label style="color: #000;" class="control-label col-sm-2">File 5:</label>
                        <div class="col-sm-10">
                        <input name="attachment5" size="35" type="file"></div>
                        </div> -->
                        <div class="clear clearfix"></div>
                        <p></p>
                    </span>
                    </div>  
                  </div>
                </div>
                <div class="panel panel-primary">
                  <div class="panel-heading"><strong style="color: #000;">Invoice</strong></div>
                  <div class="panel-body">
                    <div class="total_price-float">
                        <div class="col3 cost-per-page" style="color: #000;">Cost Per Page: <span style="">$</span>
                        <div style="color: #000;" class="per_page_price" id="per_page_price">0.00</div>
                        </div>
                        <div class="col3 discount-price" style="color: #000;">Discount: <span class="discount_per">
                        <div class="discount_percent" id="discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 discount-price" style="color: #000;">Ref: <span style="" class="ref_discount_per">
                        <div class="discount_percent" id="ref_discount_symbol" style="color: #000;">0 %</div>
                        <p></p></span></div>
                        <div class="col3 total-price" style="color: #000;">Total Price: $<span class="total">
                        <div class="total_price" id="total_price" style="color: #000;">0.00</div>
                        <p>
                        </p><div class="clear clearfix"></div>
                        </span></div>
                        <p><span class="discount_percent" id="get_ref_amount" style=""></span><span class="discount_percent" id="get_real_dis_amount">0</span><span class="total_price1" id="total_price1" style="">0</span>
                        </p><div style="" id="getif_discount_value"></div>
                        <div class="clear clearfix"></div>
                    </div>
                    <input type="hidden" name="baseprice" id="baseprice" value="0">
                    <input type="hidden" name="urgencyholder" id="urgencyholder" value="0">
                    <input type="hidden" name="qualityholder" id="qualityholder" value="0">
                    <input type="hidden" name="spacingholder" id="spacingholder" value="0">
                    <input type="hidden" name="levelholder" id="levelholder" value="0">
                    <input type="hidden" name="total_p" id="total_p" value="0">
                    <button type="submit" class="btn btn-default">Submit</button>
                  </div>
                </div>
              </div>
              </form>
            </div>
        </div><!-- /.hero-content -->
    </div>
</div>
@endsection