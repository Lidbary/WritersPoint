@extends('layouts.landing')

@section('content')
<!-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="main-wrapper">
    <div class="main">
        <div class="hero-content">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-6">
                        <h1>Write Quality. Get Paid Competitively</h1>
                        <h2>Home of Freelance Writers! We offer competitive industry rates for our writers! We are currently hiring, check out our registration page and sign up today!.</h2>

                        <a href="create-resume.html" class="hero-content-action">Sign Up Now</a>
                    </div><!-- /.col-* -->

                    <div class="col-sm-6 col-md-5 col-md-offset-1">
                        <div class="hero-content-carousel">
                            <h2 style="text-align:center">Please sign in</h2>

                         
                  
                     <form role="form" method="POST" action="{{ url('/login') }}">
                     {{ csrf_field() }}
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="form-login-username">Email</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                        </div><!-- /.form-group -->

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="form-login-password">Password</label>
                            <input id="password" type="password" class="form-control" name="password">

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                        </div><!-- /.form-group -->

                        <div class="checkbox">
                            <label><input type="checkbox" name="remember"> Keep me signed in</label>

                            <a href="{{url('/password/reset')}}" class="link-not-important pull-right">Forgot Password</a>
                        </div><!-- /.checkbox -->

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary btn-block">Sign in</button>
                        </div><!-- /.form-group -->

                        <hr>

                    
                    </form>
            
           
                            
                        </div><!-- /.hero-content-content -->
                    </div><!-- /.col-* -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.hero-content -->
    </div>
</div>
@endsection
