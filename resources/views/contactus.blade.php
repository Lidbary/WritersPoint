@extends('layouts.landing')

@section('content')
       
<div class="main-wrapper">
    <div class="main">
        <div class="hero-content">
            <div class="container">
            	<div class="row">
    					
    					    					<div class="col-sm-12">
    					     
    						<div class="lined">
		    					<h2><span class="light">Contact</span> Us</h2>
		    							    					<h5>Contact Form</h5>
		    							    					<span class="bolded-line"></span>
		    				</div>
		    				 
		    				<div class="row">
<div class="col-sm-5">
<div id="contacts">
<div role="form" class="wpcf7" id="wpcf7-f441-p58-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/contact-us#wpcf7-f441-p58-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="441"><br>
<input type="hidden" name="_wpcf7_version" value="4.3"><br>
<input type="hidden" name="_wpcf7_locale" value="en_US"><br>
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f441-p58-o1"><br>
<input type="hidden" name="_wpnonce" value="d211aeb142">
</div>
<p>Your Name (required)<br>
    <span class="wpcf7-form-control-wrap your-name"><input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false"></span> </p>
<p>Your Email (required)<br>
    <span class="wpcf7-form-control-wrap your-email"><input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false"></span> </p>
<p>Subject<br>
    <span class="wpcf7-form-control-wrap your-subject"><input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false"></span> </p>
<p>Your Message<br>
    <span class="wpcf7-form-control-wrap your-message"><textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea></span> </p>
<p><input type="submit" value="Send" class="wpcf7-form-control wpcf7-submit"><img class="ajax-loader" src="http://www.writerspoint.com/wp-content/plugins/contact-form-7/images/ajax-loader.gif" alt="Sending ..." style="visibility: hidden;"></p>
<div class="wpcf7-response-output wpcf7-display-none"></div>
</form>
</div>
</div>
</div>
<div class="col-sm-4">
<strong>Contact Information:</strong><br>
We are always ready to help you!<br>
Email: info@writerspoint.com<br>
Website: www.writerspoint.com<br>
<img src="http://www.buyessaytoday.com/wp-content/uploads/2014/01/c-1024x683.jpg" alt="" style="width:100%;"><p></p>
<p><img src="http://www.buyessaytoday.com/wp-content/uploads/2014/01/cc-1024x372.jpg" alt="" style="width:100%;">
</p></div>
</div>
<!-- <p><a onclick="chatWin = window.open('http://messenger.providesupport.com/messenger/aone.html', 'chatWin', 'toolbar=no, directories=no, location=no, status=no, menubar=no, resizable=no, scrollbars=no, width=600, height=500'); chatWin.focus(); return false;" href="#"><img style="max-width:100%;" src="http://www.writerspoint.com/wp-content/uploads/2015/05/247-support.jpg" alt="buy cheap essays 24/7"></a></p> -->
    					</div><!-- /page -->
    					
    					<div class="col-sm-9">
		    				<div class="divide-line">
		    					<div class="icon icons-divider-4"></div>
		    				</div>
		    			</div>
		    			
		    			    					
    				</div>
            </div><!-- /.container -->
        </div><!-- /.hero-content -->
    </div>
</div>
@endsection

