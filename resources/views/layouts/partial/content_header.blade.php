      @if(Session::has('success_message'))
        <div class="uk-alert uk-alert-success">
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            {!! session('success_message') !!}
        </div>
     @endif
     @if(Session::has('error_message'))
        <div class="uk-alert uk-alert-danger">
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            {!! session('error_message') !!}
        </div>
     @endif