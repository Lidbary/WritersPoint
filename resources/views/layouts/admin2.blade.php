<!doctype html>
<!--[if lte IE 9]> 
<html class="lte-ie9" lang="en">
  <![endif]-->
  <!--[if gt IE 9]><!--> 
  <html lang="en">
    <!--<![endif]-->
    <!-- Mirrored from altair_html.tzdthemes.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 12 Jul 2016 09:48:57 GMT -->
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Remove Tap Highlight on Windows Phone IE -->
      <meta name="msapplication-tap-highlight" content="no"/>
      <link rel="icon" type="image/png" href="assets/img/favicon-16x16.png" sizes="16x16">
      <link rel="icon" type="image/png" href="assets/img/favicon-32x32.png" sizes="32x32">
      <title>Dashboard</title>
      <!-- additional styles for plugins -->
      {!! Html::style('admin/bower_components/fullcalendar/dist/fullcalendar.min.css') !!}
      {!! Html::style('admin/bower_components/weather-icons/css/weather-icons.min.css') !!}
      {!! Html::style('admin/bower_components/metrics-graphics/dist/metricsgraphics.css') !!}
      {!! Html::style('admin/bower_components/chartist/dist/chartist.min.css') !!}
      {!! Html::style('admin/bower_components/uikit/css/uikit.almost-flat.min.css') !!}
      {!! Html::style('admin/assets/icons/flags/flags.min.css') !!}
      {!! Html::style('admin/assets/css/main.min.css') !!}
      {!! Html::style('admin/assets/css/themes/themes_combined.min.css') !!}
      
      <!-- matchMedia polyfill for testing media queries in JS -->
      <!--[if lte IE 9]>
      <script type="text/javascript" src="bower_components/matchMedia/matchMedia.js"></script>
      <script type="text/javascript" src="bower_components/matchMedia/matchMedia.addListener.js"></script>
      <link rel="stylesheet" href="assets/css/ie.css" media="all">
      <![endif]-->
    </head>
    <body class="sidebar_main_open sidebar_main_swipe header_double_height">
      <!-- main header -->
      <header id="header_main">
        <div class="header_main_content">
          <nav class="uk-navbar">
            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
            <span class="sSwitchIcon"></span>
            </a>
            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
            <span class="sSwitchIcon"></span>
            </a>
            <div id="menu_top_dropdown" class="uk-float-left uk-hidden-small">
              <div class="uk-button-dropdown" data-uk-dropdown="{mode:'click'}">
                <a href="#" class="top_menu_toggle"><i class="material-icons md-24">&#xE8F0;</i></a>
                <div class="uk-dropdown uk-dropdown-width-3">
                  <div class="uk-grid uk-dropdown-grid">
                    <div class="uk-width-2-3">
                      <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-bottom uk-text-center">
                        <a href="{{url('mails')}}" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-light-green-600">&#xE158;</i>
                        <span class="uk-text-muted uk-display-block">Mailbox</span>
                        </a>
                        <a href="page_invoices.html" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-purple-600">&#xE53E;</i>
                        <span class="uk-text-muted uk-display-block">Invoices</span>
                        </a>
                        <a href="page_chat.html" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-cyan-600">&#xE0B9;</i>
                        <span class="uk-text-muted uk-display-block">Chat</span>
                        </a>
                        <a href="page_scrum_board.html" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-red-600">&#xE85C;</i>
                        <span class="uk-text-muted uk-display-block">Scrum Board</span>
                        </a>
                        <a href="page_snippets.html" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-blue-600">&#xE86F;</i>
                        <span class="uk-text-muted uk-display-block">Snippets</span>
                        </a>
                        <a href="page_user_profile.html" class="uk-margin-top">
                        <i class="material-icons md-36 md-color-orange-600">&#xE87C;</i>
                        <span class="uk-text-muted uk-display-block">User profile</span>
                        </a>
                      </div>
                    </div>
                    <div class="uk-width-1-3">
                      <ul class="uk-nav uk-nav-dropdown uk-panel">
                        <li class="uk-nav-header">Components</li>
                        <li><a href="components_accordion.html">Accordions</a></li>
                        <li><a href="components_buttons.html">Buttons</a></li>
                        <li><a href="components_notifications.html">Notifications</a></li>
                        <li><a href="components_sortable.html">Sortable</a></li>
                        <li><a href="components_tabs.html">Tabs</a></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="uk-navbar-flip">
              <ul class="uk-navbar-nav user_actions">
                <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                <li><a href="#" id="main_search_btn" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE8B6;</i></a></li>
                <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                  <a href="#" class="user_action_icon"><i class="material-icons md-24 md-light">&#xE7F4;</i><span class="uk-badge">16</span></a>
                  <div class="uk-dropdown uk-dropdown-xlarge">
                    <div class="md-card-content">
                      <ul class="uk-tab uk-tab-grid" data-uk-tab="{connect:'#header_alerts',animation:'slide-horizontal'}">
                        <li class="uk-width-1-2 uk-active"><a href="#" class="js-uk-prevent uk-text-small">Messages (12)</a></li>
                        <li class="uk-width-1-2"><a href="#" class="js-uk-prevent uk-text-small">Alerts (4)</a></li>
                      </ul>
                      <ul id="header_alerts" class="uk-switcher uk-margin">
                        <li>
                          <ul class="md-list md-list-addon">
                            <li>
                              <div class="md-list-addon-element">
                                <span class="md-user-letters md-bg-cyan">xl</span>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Nobis temporibus.</a></span>
                                <span class="uk-text-small uk-text-muted">Doloribus magnam vitae et eius doloribus.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_07_tn.png" alt=""/>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Ab nulla quidem.</a></span>
                                <span class="uk-text-small uk-text-muted">Molestias omnis et aut consequatur excepturi harum quas consequatur.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <span class="md-user-letters md-bg-light-green">om</span>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Sed rerum.</a></span>
                                <span class="uk-text-small uk-text-muted">Maxime a itaque voluptatem dolore modi nobis suscipit aliquam voluptatem neque.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_02_tn.png" alt=""/>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Quidem quae optio.</a></span>
                                <span class="uk-text-small uk-text-muted">Quia animi at ut aut consequuntur.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <img class="md-user-image md-list-addon-avatar" src="assets/img/avatars/avatar_09_tn.png" alt=""/>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading"><a href="pages_mailbox.html">Explicabo dolorum nemo.</a></span>
                                <span class="uk-text-small uk-text-muted">Repellat aut illum consequatur et atque consequuntur placeat.</span>
                              </div>
                            </li>
                          </ul>
                          <div class="uk-text-center uk-margin-top uk-margin-small-bottom">
                            <a href="page_mailbox.html" class="md-btn md-btn-flat md-btn-flat-primary js-uk-prevent">Show All</a>
                          </div>
                        </li>
                        <li>
                          <ul class="md-list md-list-addon">
                            <li>
                              <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-warning">&#xE8B2;</i>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading">Reprehenderit voluptas.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Sit cumque eaque odit.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-success">&#xE88F;</i>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading">Quidem et.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Exercitationem assumenda mollitia ut et excepturi.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-danger">&#xE001;</i>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading">Sunt velit corrupti.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Praesentium nam qui voluptas et aliquid.</span>
                              </div>
                            </li>
                            <li>
                              <div class="md-list-addon-element">
                                <i class="md-list-addon-icon material-icons uk-text-primary">&#xE8FD;</i>
                              </div>
                              <div class="md-list-content">
                                <span class="md-list-heading">Nesciunt eaque qui.</span>
                                <span class="uk-text-small uk-text-muted uk-text-truncate">Laudantium a et non nam quos accusantium.</span>
                              </div>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                  </div>
                </li>
                <li data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                  <a href="#" class="user_action_image"><img class="md-user-image" src="admin/assets/img/avatars/avatar_11_tn.png" alt=""/></a>
                  <div class="uk-dropdown uk-dropdown-small">
                    <ul class="uk-nav js-uk-prevent">
                      <li><a href="page_user_profile.html">My profile</a></li>
                      <li><a href="page_settings.html">Settings</a></li>
                      <li><a href="{{url('logout')}}">Logout</a></li>
                    </ul>
                  </div>
                </li>
              </ul>
            </div>
          </nav>
        </div>
       <div class="header_main_search_form">
          <i class="md-icon header_main_search_close material-icons">&#xE5CD;</i>
          <form class="uk-form uk-autocomplete" data-uk-autocomplete="{source:'data/search_data.json'}">
            <input type="text" class="header_main_search_input" />
            <button class="header_main_search_btn uk-button-link"><i class="md-icon material-icons">&#xE8B6;</i></button>
            <script type="text/autocomplete">
              <ul class="uk-nav uk-nav-autocomplete uk-autocomplete-results">
                  @{{~items}}
                  <li data-value="@{{ $item.value }}">
                      <a href="@{{ $item.url }}">
                          @{{ $item.value }}<br>
                          <span class="uk-text-muted uk-text-small">@{{{ $item.text }}}</span>
                      </a>
                  </li>
                  @{{/items}}
              </ul>
            </script>
          </form>
        </div>
      </header>
      <!-- main header end -->
      <!-- main sidebar -->
      <aside id="sidebar_main">
        <div class="sidebar_main_header">
          <div class="sidebar_logo">
            <a href="{{url('/home')}}" class="sSidebar_hide sidebar_logo_large" style="font-size:20px; font-family:Impact, Charcoal, sans-serif; color:#009688">
            {{Auth::user()->company_name}} Law Firm
            <!-- <img class="logo_light" src="assets/img/logo_main_white.png" alt="" height="15" width="71"/> -->
            </a>
            <a href="index-2.html" class="sSidebar_show sidebar_logo_small">
            <img class="logo_regular" src="assets/img/logo_main_small.png" alt="" height="32" width="32"/>
            <img class="logo_light" src="assets/img/logo_main_small_light.png" alt="" height="32" width="32"/>
            </a>
          </div>
        
        </div>
        <div class="menu_section">
          <ul>
            <li class="current_section" title="Dashboard">
              <a href="{{url('home')}}">
              <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
              <span class="menu_title">Dashboard</span>
              </a>
            </li>
            <li title="Mailbox">
              <a href="{{url('mails')}}">
              <span class="menu_icon"><i class="material-icons">&#xE158;</i></span>
              <span class="menu_title">Mailbox</span>
              </a>
            </li>
            <li title="Invoices">
              <a href="{{url('invoices')}}">
              <span class="menu_icon"><i class="material-icons">&#xE53E;</i></span>
              <span class="menu_title">Invoices</span>
              </a>
            </li>
            <li title="Chat">
              <a href="{{url('notes')}}">
              <span class="menu_icon"><i class="material-icons">&#xE0B9;</i></span>
              <span class="menu_title">Notes</span>
              </a>
            </li>
            <li title="Snippets">
              <a href="{{url('matters')}}">
              <span class="menu_icon"><i class="material-icons">&#xE86F;</i></span>
              <span class="menu_title">Matters</span>
              </a>
            </li>
            <li title="User Profile">
              <a href="{{url('documents')}}">
              <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
              <span class="menu_title">Documents</span>
              </a>
            </li>
            <li title="Forms">
              <a href="{{url('calendar')}}">
              <span class="menu_icon"><i class="material-icons">&#xE8D2;</i></span>
              <span class="menu_title">Calendar</span>
              </a>
            </li>
            <li title="Layout">
              <a href="{{url('tasks')}}">
              <span class="menu_icon"><i class="material-icons">&#xE8F1;</i></span>
              <span class="menu_title">Activities</span>
              </a>
            
            </li>
            <li title="Kendo UI Widgets">
              <a href="{{url('activities')}}">
              <span class="menu_icon"><i class="material-icons">&#xE1BD;</i></span>
              <span class="menu_title">Tasks</span>
              </a>
            
            </li>
            <li title="Components">
              <a href="{{url('accounts')}}">
              <span class="menu_icon"><i class="material-icons">&#xE87B;</i></span>
              <span class="menu_title">Accounts</span>
              </a>
            </li>
          </ul>
        </div>
      </aside>
      <!-- main sidebar end -->

    @yield('content')
     <footer id="footer">
        &copy; 2016 <a href="#">Ewakili</a>, All rights reserved.
     </footer>

   <script>
        WebFontConfig = {
            google: {
                families: [
                    'Source+Code+Pro:400,700:latin',
                    'Roboto:400,300,500,700,400italic:latin'
                ]
            }
        };
        (function() {
            var wf = document.createElement('script');
            wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
            '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
            wf.type = 'text/javascript';
            wf.async = 'true';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(wf, s);
        })();
      </script>
      {!! Html::script('admin/assets/js/common.min.js') !!}
      {!! Html::script('admin/assets/js/uikit_custom.min.js') !!}
      {!! Html::script('admin/assets/js/altair_admin_common.min.js') !!}
      @yield('page-script')
      {!! Html::script('admin/bower_components/d3/d3.min.js') !!}
      {!! Html::script('admin/bower_components/metrics-graphics/dist/metricsgraphics.min.js') !!}
      {!! Html::script('admin/bower_components/chartist/dist/chartist.min.js') !!}
      {!! Html::script('admin/bower_components/maplace-js/dist/maplace.min.js') !!}
      {!! Html::script('admin/bower_components/peity/jquery.peity.min.js') !!}
      {!! Html::script('admin/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') !!}
      {!! Html::script('admin/bower_components/countUp.js/dist/countUp.min.js') !!}
      {!! Html::script('admin/bower_components/handlebars/handlebars.min.js') !!}
      {!! Html::script('admin/assets/js/custom/handlebars_helpers.min.js') !!}
      {!! Html::script('admin/bower_components/clndr/clndr.min.js') !!}
      {!! Html::script('admin/bower_components/fitvids/jquery.fitvids.js') !!}     
      {!! Html::script('admin/assets/js/pages/dashboard.min.js') !!}
      
      <script>
        $(function() {
            if(isHighDensity) {
                // enable hires images
                altair_helpers.retina_images();
            }
            if(Modernizr.touch) {
                // fastClick (touch devices)
                FastClick.attach(document.body);
            }
        });
        $window.load(function() {
            // ie fixes
            altair_helpers.ie_fix();
        });
      </script>
    
      <script>
        $(function() {     
        
        
            // hide style switcher
            $document.on('click keyup', function(e) {
                if( $switcher.hasClass('switcher_active') ) {
                    if (
                        ( !$(e.target).closest($switcher).length )
                        || ( e.keyCode == 27 )
                    ) {
                        $switcher.removeClass('switcher_active');
                    }
                }
            });
        
            // get theme from local storage
            if(localStorage.getItem("altair_theme") !== null) {
                $theme_switcher.children('li[data-app-theme='+localStorage.getItem("altair_theme")+']').click();
            }
        
        
        // toggle mini sidebar
        
            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_mini") !== null && localStorage.getItem("altair_sidebar_mini") == '1') || $body.hasClass('sidebar_mini')) {
                $mini_sidebar_toggle.iCheck('check');
            }
        
            $mini_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_mini", '1');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                });
        
        // toggle slim sidebar
        
            // change input's state to checked if mini sidebar is active
            if((localStorage.getItem("altair_sidebar_slim") !== null && localStorage.getItem("altair_sidebar_slim") == '1') || $body.hasClass('sidebar_slim')) {
                $slim_sidebar_toggle.iCheck('check');
            }
        
            $slim_sidebar_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_sidebar_slim", '1');
                    localStorage.removeItem('altair_sidebar_mini');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_sidebar_slim');
                    location.reload(true);
                });
        
        // toggle boxed layout
        
            if((localStorage.getItem("altair_layout") !== null && localStorage.getItem("altair_layout") == 'boxed') || $body.hasClass('boxed_layout')) {
                $boxed_layout_toggle.iCheck('check');
                $body.addClass('boxed_layout');
                $(window).resize();
            }
        
            $boxed_layout_toggle
                .on('ifChecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.setItem("altair_layout", 'boxed');
                    location.reload(true);
                })
                .on('ifUnchecked', function(event){
                    $switcher.removeClass('switcher_active');
                    localStorage.removeItem('altair_layout');
                    location.reload(true);
                });
        
        // main menu accordion mode
            if($sidebar_main.hasClass('accordion_mode')) {
                $accordion_mode_toggle.iCheck('check');
            }
        
            $accordion_mode_toggle
                .on('ifChecked', function(){
                    $sidebar_main.addClass('accordion_mode');
                })
                .on('ifUnchecked', function(){
                    $sidebar_main.removeClass('accordion_mode');
                });
        
        
        });
      </script>
</body>
</html>
