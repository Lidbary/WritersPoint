@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">
          <!-- statistics (small charts) -->
          <div class="uk-alert uk-alert-info">
            <strong>Hello Clinet!</strong><br>
            Dashboard Content coming soon
        </div>

        </div>
    </div>


@endsection
 @section('page-script')
<!-- page specific plugins -->
   {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
