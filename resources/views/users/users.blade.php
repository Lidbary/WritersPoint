@extends('layouts.admin')

@section('content')
 <div id="page_content">
        <div id="page_content_inner">


            <h4 class="heading_a uk-margin-bottom">Manage Users </h4>
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_individual_search" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                       
                        <tr>
                           <th>Name</th>
                            <th>Email</th>
                            <th>User Level</th>
                            <th>Actions</th>
                        </tr>
                        
                        </thead>

                        <tfoot>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>User Level</th>
                            <th>Actions</th>
                        </tr>
                        </tfoot>

                        <tbody>
                         @foreach ($users as $user)
                        <tr>
                            <td>{{$user->firstname}} {{$user->lastname}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->name}}</td>
                            <td><a href="">more..</a></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

    </div>
    </div>


@endsection
 @section('page-script')
    {!! Html::script('admin/bower_components/datatables/media/js/jquery.dataTables.min.js') !!}
    {!! Html::script('admin/bower_components/datatables-colvis/js/dataTables.colVis.js') !!}
    {!! Html::script('admin/bower_components/datatables-tabletools/js/dataTables.tableTools.js') !!}
    {!! Html::script('admin/assets/js/custom/datatables_uikit.min.js') !!}
    {!! Html::script('admin/assets/js/pages/plugins_datatables.min.js') !!}
    @stop
