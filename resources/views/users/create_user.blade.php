@extends('layouts.admin')

@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_b uk-margin-bottom">Create New User  <a style="font-size:14px; margin-left:20px" href="{{URL::route('users')}}">View all Users>>>></a></h3>

            <div class="md-card" style="background:#F0FFFF">
             @if(Session::has('success_message'))
                          <div class="uk-alert uk-alert-success">{!! session('success_message') !!}</div>
                         @endif
              <div class="md-card-content large-padding">
             <form id="form_validation" class="uk-form-stacked" method="POST" action="{{URL::route('create_user')}}">
              {{ csrf_field() }}
                <div class="uk-grid" data-uk-grid-margin>
                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Username<span class="req">*</span></label>
                      <input type="text" name="username" class="md-input" />
                       @if ($errors->has('username'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('username') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>
                   
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Email<span class="req">*</span></label>
                      <input type="email" name="email" class="md-input" />
                       @if ($errors->has('email'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('email') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>
                 
                </div>

                  <div class="uk-grid" data-uk-grid-margin>
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Firstname<span class="req">*</span></label>
                      <input type="text" name="firstname" class="md-input" />
                       @if ($errors->has('firstname'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('firstname') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>

                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Lastname<span class="req">*</span></label>
                      <input type="text" name="lastname" class="md-input" />
                       @if ($errors->has('lastname'))
                        <span class="help-block" style="color:#a94442">
                        <strong>{{ $errors->first('lastname') }}</strong>
                     </span>
                      @endif
                    </div>
                  </div>
                   
                </div>

                 <div class="uk-grid" data-uk-grid-margin>
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Password<span class="req">*</span></label>
                     <input type="password" name="password" class="md-input">
                         @if ($errors->has('password'))
                                    <span class="help-block" style="color:#a94442">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                    </div><!-- /.form-group -->
                    </div>
                 

                   <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">Confirm Password<span class="req">*</span></label>
                      <input type="password" name="password_confirmation" class="md-input">
                         @if ($errors->has('password_confirmation'))
                                    <span class="help-block" style="color:#a94442">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                    </div>
                  </div>
                   
                </div>
                 <div class="uk-grid" data-uk-grid-margin>
                 <div class="uk-width-medium-1-2">
                    <div class="parsley-row">
                      <label for="fullname">User Level<span class="req">*</span></label>
                      <select id="select_demo_3" name="user_level" class="md-input">
                       <option value="" disabled selected hidden>Select...</option>
                        <option value="admin">Admin</option>
                        <option value="editor">Editor</option>
                        <option value="writer">Writer</option>
                        <!-- <option value="client">Client</option> -->
                                
                            </select>
                            @if ($errors->has('user_level'))
                            <span class="help-block" style="color:#a94442">
                             <strong>{{ $errors->first('user_level') }}</strong>
                            </span>
                            @endif
                    </div>
                  </div>
                </div>
               
                <div class="uk-grid">
                  <div class="uk-width-1-1">
                    <button type="submit" class="md-btn md-btn-primary">Create User</button>
                  </div>
                </div>
              </form>
                </div>
            </div>
        </div>
    </div>


@endsection
 @section('page-script')
   {!! Html::script('admin/assets/js/kendoui_custom.min.js') !!}
   {!! Html::script('admin/assets/js/pages/kendoui.min.js') !!}
    @stop
