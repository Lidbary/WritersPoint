<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
    'doctype', 'order_title', 'order_level', 'max_bid', 'no_of_pages', 'spacing', 'digital_sources', 'discipline', 'deadline', 'client_price', 'track_id', 'citation', 'no_of_sources', 'instructions', 'approved', 'paid', 'banned', 'available', 'assigned', 'removed_order', 'fined','quality','urgency','subject_area','style','description','dictionary','file','download_file'
    ];

    public function users()
	{
	    return $this->belongsToMany('App\User');
	}
}
