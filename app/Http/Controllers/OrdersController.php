<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\includes\Adaptive;
use App\Http\Requests;
use Session;
use App\Order;
use Redirect;
use Validator;
use Auth;
use DB;
use App\User;
use App\File;
use App\Country;
use App\Product;
use App\Urgency;
use App\Role;
use App\RoleUser;
use App\Pricing;
use Storage;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products=Product::select('id','name')->get();
        $urgency=Urgency::select('id','name')->get();
        return view('orders.create_order',compact('products','urgency'));
    }

    public function manage()
    {
        if(Auth::user()->hasrole('admin')){
            $orders=Order::all();            
        }else{
            $orders=Order::where('user_id','=',Auth::user()->id)->get();
        }
        return view('orders.manage-orders', ['orders'=>$orders,'available'=>[]]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'topic' => 'required',
            'product_id'=>'required',
            'urgency'=>'required',
            'quality'=>'required',
            'spacing'=>'required',
            'pages'=>'required',
            'academic_level'=>'required',
            'subject_area'=>'required',
            'style'=>'required',
            'source'=>'required',
            'dictionary'=>'required',
            'description'=>'required',
            'attachment1'=>'required',
        );
        $validator = Validator::make($input_data = $request->all(), $rules);
        // dd($input_data);
        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
  

        $neworder = New Order;
        $neworder->user_id = Auth::user()->id;
        $neworder->order_title=$input_data['topic'];
        $neworder->doctype=$input_data['product_id'];
        $neworder->urgency=$input_data['urgency'];
        $neworder->quality=$input_data['quality'];
        $neworder->spacing=$input_data['spacing'];
        $neworder->no_of_pages=$input_data['pages'];
        $neworder->order_level=$input_data['academic_level'];
        $neworder->subject_area=$input_data['subject_area'];
        $neworder->style=$input_data['style'];
        $neworder->no_of_sources=$input_data['source'];
        $neworder->dictionary=$input_data['dictionary'];
        $neworder->description=$input_data['description'];
        $neworder->client_price=$input_data['total_p'];
        if($file = $request->hasFile('attachment1')) {
            $file = $request->file('attachment1') ;
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/uploads/images/' ;
            $file->move($destinationPath,$fileName);
            $neworder->file = $fileName ;
        }
        $neworder->save();
        return Redirect::route('preview_another',$neworder->id);
        // Session::flash('success_message', 'order created successifuly!');
        return redirect()->back();
    }
}

public function upload(Request $request) 
{
     $rules = array(
            'name'       => 'required',
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
        if($file = $request->hasFile('name')) {
            $order = Order::findOrFail($input_data['order_id']);    
            $file = $request->file('name') ;
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/uploads/images/' ;
            $file->move($destinationPath,$fileName);
            // dd($fileName);
            $order->download_file = $fileName ;
            $order->approved = 1 ;
        }
        $order->save();

        Session::flash('success_message', 'file uploaded successifuly!');
        return redirect()->back();
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $files = "";
        $vieworder = Order::findOrFail($id);
        return view('orders.order_details', ['vieworder'=>$vieworder, 'files'=>$files]);
    }


    public function assignorder(Request $request) {
        $input_data = $request->all();

        $order = Order::where('id', '=', $input_data['order_id'])->firstOrFail();
        $user = User::where('id', '=', $input_data['user_id'])->firstOrFail();

        $user->orders()->attach($order);

        Session::flash('success_message', 'Order assigned successifuly!');
        return redirect()->back();
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function order(){
        $countries=DB::table('country')->select('id', 'name', 'nicename')->get();
        $products=Product::select('id','name')->get();
        $urgency=Urgency::select('id','name')->get();
        return view('auth.register',compact('countries','products','urgency'));
    }

    public function postorder(Request $request){
        $rules = array(
            'username'       => 'required',
            'email'      => 'required|email',
            'firstname'      => 'required',
            'lastname'      => 'required',
            'country'      => 'required',
            'pwd' => 'required|min:6',
            'phone_number' => 'required',
            'topic' => 'required',
            'product_id'=>'required',
            'urgency'=>'required',
            'quality'=>'required',
            'spacing'=>'required',
            'pages'=>'required',
            'academic_level'=>'required',
            'subject_area'=>'required',
            'style'=>'required',
            'source'=>'required',
            'dictionary'=>'required',
            'description'=>'required',
            'attachment1'=>'required',
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        }
        $input_data = $request->all();
        $newuser = New User;
        $newuser->username = $input_data['username'];
        $newuser->email = $input_data['email'];
        $newuser->firstname = $input_data['firstname'];
        $newuser->lastname = $input_data['lastname'];
        $newuser->password = bcrypt($input_data['pwd']);
        $newuser->mobile_number=$input_data['phone_number'];
        $newuser->country = $input_data['country'];
        $newuser->save();
        $neworder= New Order;
        $neworder->user_id=$newuser->id;
        $neworder->order_title=$input_data['topic'];
        $neworder->doctype=$input_data['product_id'];
        $neworder->urgency=$input_data['urgency'];
        $neworder->quality=$input_data['quality'];
        $neworder->spacing=$input_data['spacing'];
        $neworder->no_of_pages=$input_data['pages'];
        $neworder->order_level=$input_data['academic_level'];
        $neworder->subject_area=$input_data['subject_area'];
        $neworder->style=$input_data['style'];
        $neworder->no_of_sources=$input_data['source'];
        $neworder->dictionary=$input_data['dictionary'];
        $neworder->description=$input_data['description'];
        $neworder->client_price=$input_data['total_p'];
        if($file = $request->hasFile('attachment1')) {
            $file = $request->file('attachment1') ;
            $fileName = $file->getClientOriginalName() ;
            $destinationPath = public_path().'/uploads/images/' ;
            $file->move($destinationPath,$fileName);
            $neworder->file = $fileName ;
        }
        $neworder->save();
        $user_role=new RoleUser;
        $user_role->user_id=$newuser->id;
        $user_role->role_id=4;
        $user_role->save();
        return Redirect::route('preview_order',['id' => $neworder->id]);
    }

    public function orderdata(Request $request){
         $input_data = $request->all();
         if(isset($input_data['product_id']) && isset($input_data['urgency'])){
            $pricing=Pricing::where('product_id','=',$input_data['product_id'])->where('urgency','=',$input_data['urgency'])->firstOrFail();
            echo $pricing->price;
         }elseif(isset($input_data['product_id']) && isset($input_data['quality'])){
            $pricing=Pricing::where('product_id','=',$input_data['product_id'])->where('quality','=',$input_data['quality'])->firstOrFail();
            echo $pricing->price;
         }elseif(isset($input_data['product_id']) && isset($input_data['spacing'])){
            $pricing=Pricing::where('product_id','=',$input_data['product_id'])->where('spacing','=',$input_data['spacing'])->firstOrFail();
            echo $pricing->price;
         }elseif(isset($input_data['product_id']) && isset($input_data['academic_level'])){
            $pricing=Pricing::where('product_id','=',$input_data['product_id'])->where('academic_level','=',$input_data['academic_level'])->firstOrFail();
            echo $pricing->price;
         }
        elseif(isset($input_data['product_id'])){
            $product=Product::findOrFail($input_data['product_id']);
            echo $product->price;
         }
    }

    public function previeworder($id){
        // $input_data = $request->all();
        // $id=$input_data['id'];
        $order=Order::findOrFail($id);
        if($order->paid!=1){
        $user=User::findOrFail($order->user_id);
        $product=Product::findOrFail($order->doctype);
        return view('orders.preview_order',compact('order','user','product'));            
        }else{
            return redirect()->back();
        }
    }

    public function previewAnother($id){
        $order=Order::findOrFail($id);
        if($order->paid!=1){
        $user=User::findOrFail($order->user_id);
        $product=Product::findOrFail($order->doctype);
        return view('orders.preview_another_order',compact('order','user','product'));            
        }else{
            return redirect()->back();
        }
    }
}
