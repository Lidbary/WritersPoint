<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\User;
use App;
use Mail;
use DB;

class PaymentsController extends Controller
{
    public function index($id){
            // return view('public.payments.index');
      $experience=Experience::findOrFail($id);
      $countries=Country::all();
      if(!$experience)
      {
        return redirect()->back();
      }
        return view('public.payments.index',compact('countries'));
    }

    public function paypalIndex($id){
            // return "2";
      $experience=Experience::findOrFail($id);
      $countries=Country::all();
      if(!$experience)
      {
        return redirect()->back();
      }
        return view('public.payments.index2',compact('countries'));
    }

        public function LangDe(){
            $lag= 'de';
        App::setLocale("German");
        return redirect()->route('home.index');



    }
    public function LangEn(){
        App::setLocale('en');

        return redirect()->back();
    }

    public function redirect(Request $request)
    {
      if($request->input('error'))
      {
        $experience=\Session::get('e_id');
        \Session::flash('payment_error',$request->input('error_description'));
        return redirect()->route('experience.payment-edit',$experience);
      }
      else
      {
        //return $request->input('code');
  //$CLIENT_ID=env('PLATFORM_KEY');
  $CLIENT_ID='ca_8FsrkdOrmmlzaETO41aEvsj4RXdMqM37';
  //$API_KEY=env('STRIPE_SK');
  $API_KEY='sk_test_4yKye7EUOXIJnzphQUdNaiHl';
  $TOKEN_URI='https://connect.stripe.com/oauth/token';
    $code = $request->input('code');
    $token_request_body = array(
      'client_secret' => $API_KEY,
      'grant_type' => 'authorization_code',
      'client_id' => $CLIENT_ID,
      'code' => $code,
    );
    $req = curl_init($TOKEN_URI);
    $path = base_path('config/cacert.pem');
    curl_setopt($req, CURLOPT_SSL_VERIFYPEER, FALSE);
    // curl_setopt($req, CURLOPT_CAINFO, "C:\Users\Theo\Downloads\cacert.pem");
    curl_setopt($req, CURLOPT_CAINFO, $path);
    curl_setopt($req, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($req, CURLOPT_POST, true );
    curl_setopt($req, CURLOPT_POSTFIELDS, http_build_query($token_request_body));
    // TODO: Additional error handling
    $respCode = curl_getinfo($req, CURLINFO_HTTP_CODE);
    $resp = json_decode(curl_exec($req), true);
    curl_close($req);
    // var_dump($resp);
    $stripe_details=UserStripe::create([
                    'user_id' => Auth::user()->id,
                    'access_token' => $resp['access_token'],
                    'refresh_token' => $resp['refresh_token'],
                    'token_type' => $resp['token_type'],
                    'stripe_publishable_key' =>$resp['stripe_publishable_key'],
                    'stripe_user_id' => $resp['stripe_user_id'],
                ]);
    $experience=\Session::get('e_id');
     \Session::flash('payment_success','Congratulations!! Your stripe payment has been set up successfully');
    return redirect()->route('experience.payment-edit',$experience);
      }
      
    }


    public function getPayment()
    {
        return view('pages.order');
    }

    public function postpayment(Request $request)
    {


            $validator = Validator::make($request->all(), [
            'firstname_booking' => 'required|max:255',
            'lastname_booking' => 'required|max:255',
            'email_booking' => 'required|email|max:255',
            // 'email_booking_confirmation' => 'confirmed',
            'telephone_booking' => 'required|max:255',
            'name_card_booking' => 'required|max:255',
            'country' => 'required',
            'street_1' => 'required',
            'city_booking' => 'required',
            'postal_code'  => 'required',
        ]);
            // dd($validator);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        else
        {

                  $experience=Experience::findOrFail(\Session::get('rbooking_id'));
                  $user_id=$experience->user_id;
                  $user=User::findOrFail($user_id);
                  $description=$experience->title;
                  $quantity=\Session::get('rbooking_quantity');
                  $destination_id=UserStripe::where('user_id',$user_id)->value('stripe_user_id');
                  $fee=\Session::get('rbooking_total')*0.125;
                  $vat=\Session::get('rbooking_vat');
                  // $total=\Session::get('rbooking_real_total');
                  $total=\Session::get('rbooking_total');
                  $people=\Session::get('rbooking_people');
                  $children=\Session::get('rbooking_children');
                  $stripe_fees=$total*0.03;
                  $application_fee=ceil(($vat+$fee+$stripe_fees)*100);
                  $customer_name=$request->input('firstname_booking')." ".$request->input('lastname_booking');
                  // return $access_token;
                  \Stripe\Stripe::setApiKey('sk_test_4yKye7EUOXIJnzphQUdNaiHl');
                  // \Stripe\Stripe::setApiKey("sk_test_fOipzEQI5NXQGpBDNBrKbI0B");
                    try {
                      // var_dump($user_id);
              // if ($request->input('stripeToken')=="") throw new Exception("The Stripe Token was not generated correctly");

              // $charge = \Stripe\Charge::create(array(
              //   'source'     => $request->input('stripeToken'),
              //   'amount'   => ceil($total*100),
              //   'currency' => 'eur',
              //   'description' => $description,
              //   'metadata' => [
              //   "First Name" => $request->input('firstname_booking'),
              //   "Last Name" => $request->input('lastname_booking'),
              //   "guide_id" =>$user_id,
              //   'guide_name'=>$user->name,
              //   ],
              //   // 'receipt_email' => "lidbary@gmail.com" ,          
              //   'application_fee' =>$application_fee, // amount in cents
              //   'destination' => $destination_id
              // ));
                // $payments_check=Customer::where('user_id',Auth::user()->id)->first();
                $date=date_create(\Session::get('rbooking_scheduled_date'));
                $new_date=date_format($date,"Y-m-d");
                // if (sizeof($payments_check)==0) {
                $charge = \Stripe\Customer::create(array(
                'source'     => $request->input('stripeToken'),
                // 'amount'   => ceil($total*100),
                'email'    => $request->input('email_booking'),
                // 'currency' => 'eur',
                'description' => $description,
                'metadata' => [
                "First Name" => $request->input('firstname_booking'),
                "Last Name" => $request->input('lastname_booking'),
                "guide_id" =>$user_id,
                'guide_name'=>$user->name,
                ],
                // 'receipt_email' => "lidbary@gmail.com" ,          
                // 'application_fee' =>$application_fee, // amount in cents
                // 'destination' => $destination_id
              ));
                // }
                // else
                // {
                //   $charge=\Stripe\Customer::retrieve($payments_check->customer_id);
                // }

                if (isset(Auth::user()->id)) {
                  $customer_id=Auth::user()->id;
                }
                else
                {
                  $customer_id=0;
                }


                $brand="";
                $last4="";

                $container=$charge->sources->data;
                // dd($container);
                foreach ($container as $con) {
                  $brand=$con->brand;
                  $last4=$con->last4;
                }
                // if (sizeof($payments_check)==0) {
                $custs=Customer::create([
                'user_id' =>$customer_id ,
                'customer_id' => $charge->id,
                ]);
              // }
                $order_id=Purchase::create([
                'user_id' => $user_id,
                'amount' => $total,
                'stripe_transaction_id' => "",
                'email_booking' => $request->input('email_booking'),
                'telephone_booking' => $request->input('telephone_booking'),
                'firstname_booking' =>$request->input('firstname_booking'),
                'lastname_booking' =>$request->input('lastname_booking'),
                'name_card_bookign' => $request->input('name_card_booking'),
                'country' => $request->input('country'),
                'street_1' => $request->input('street_1'),
                'street_2' =>$request->input('street_2'),
                'city_booking' =>$request->input('city_booking'),
                'state_booking' => $request->input('state_booking'),
                'postal_code' =>$request->input('postal_code'),
                'people' =>$people,
                'children' =>$children,
                'scheduled_date' => $new_date,
                'lunch' =>\Session::get('rbooking_lunch_total'),
                'breakfast' =>\Session::get('rbooking_breakfast_total'),
                'dinner' =>\Session::get('rbooking_dinner_total'),
                'pus' =>\Session::get('rbooking_pus_total'),
                'wbottle' =>\Session::get('rbooking_wbottle_total'),
                'scheduled_time' =>\Session::get('rbooking_scheduled_time'),
                'fee' => $fee,
                'vat' => $vat,
                'last_four_digits'=> $last4,
                'quantity' => $quantity,
                'stripe_fees' =>$stripe_fees,
                'brand' =>$brand,
                'customer_id' => $customer_id,
                'experience_id'=>\Session::get('rbooking_id'),
                'stripe_customer_id'=>$charge->id,
                ])->getAttribute('id'); 

                // $brand=$charge->sources->data->brand;
                $testClientAvailability=User::where('email',$request->input('email_booking'))->first();
                $is_new_client=0;
                  $new_password = "password";
                if(sizeof($testClientAvailability)<1)
                {
                        $is_new_client=1;
                        $newClient=new User();
                        $newClient->name = $customer_name;
                        $newClient->email = $request->input('email_booking');
                        $newClient->image_url  = "/wb-content/img/default-img.png";
                        $newClient->time_zone_id = 61;
                        $newClient->password = bcrypt($new_password);
                        $newClient->role = 'customer';
                        $newClient->save();
                        $customer_id=$newClient->id;
                }
                else
                {
                    $customer_id=$testClientAvailability->id;
                }

                $email_data = array(
                      // 'order_id' => $order_id ,
                      'name'  => $customer_name ,
                      // 'postal_code'=> $request->input('postal_code') ,
                      // 'street_1'=> $request->input('street_1'),
                      // 'last_four_digits'=> $last4,
                      'email'=> $request->input('email_booking'),
                      'description'=> $description,
                      // 'vat' => $vat,
                      // 'total' => $total,
                      // 'quantity' =>$quantity,
                      // 'brand' => $brand,
                      // 'city' =>$request->input('city_booking') ,
                      'people' => $people,
                      'children' => $children,
                      'scheduled_date' =>\Session::get('rbooking_scheduled_date'),
                      'scheduled_time' =>\Session::get('rbooking_scheduled_time'),
                      'guide_name'=>$user->name,
                      'password' =>$new_password,
                      'is_new_client'=>$is_new_client,
                      'payment_type' => "stripe",
                      'order_id' => $order_id,
                  );

                $order=Purchase::findOrFail($order_id);
                $order->customer_id=$customer_id;
                $order->save();
                  Mail::send('public.payments.invoice', $email_data, function ($message) use ($order) {
                  $message->to($order->email_booking,$order->firstname_booking)->subject('Dein Tourvoucher');
                });

                  Mail::send('public.payments.request_guide', $email_data, function ($message) use ($user) {
                  $message->to($user->email,$user->name)->subject('Buchungsanfrage');
                });

          /* ==========================================================================
           Initiate conversation between tour guide and user
           ========================================================================== */
        $user_one=$user->id;
        $user_two=$customer_id;
        $reply=trans('booking.message_msg3');
        // check if the conversation exists
        $sq=DB::select( DB::raw("SELECT c_id FROM conversation WHERE (user_one='$user_one' and user_two='$user_two') 
        or (user_one='$user_two' and user_two='$user_one')") );
        $time=time();
        $ip=$_SERVER['REMOTE_ADDR'];
        $conversation_reply=ConversationReply::all();
        $user_conversation= new UserConversation();
        // if the conversation does not exist, create a new one
        if (sizeof($sq)==0) {
        $conversation_id=DB::table('conversation')->insertGetId([
            'user_one'=>$user_one,
            'user_two'=>$user_two,
            'ip'=>$ip,
            'time'=>$time,
        ]); 
         $c_id=$conversation_id;
         $user_conversation=UserConversation::create([
            'user_id'=>$user_one,
            'c_id'=>$c_id,
            'read_status'=>0,
            ]);
        $user_conversation2=UserConversation::create([
            'user_id'=>$user_two,
            'c_id'=>$c_id,
            'read_status'=>1,
            ]);
        }
        else
        {
            // else update the conversation details
            $c_id=$sq[0]->c_id;
        DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_one)
            ->update(['read_status' => 0]);
         DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_two)
            ->update(['read_status' => 1]);
        $date_time=DATE('Y-m-d H:i:s');
        DB::table('conversation')
            ->where('c_id', $c_id)
            ->update(['ts' => $date_time]);
        }
        // insert the new message
        $user_conversation2=ConversationReply::create([
            'user_id_fk'=>$user_one,
            'user_id_fk_2'=>$user_two,
            'reply'=>$reply,
            'ip'=>$ip,
            'time'=>$time,
            'c_id_fk'=>$c_id,
            ]);
        /* ==========================================================================
           End of conversation between tour guide and user
           ========================================================================== */
                  return view('public.payments.confirmation',compact('order_id','quantity','brand','customer_name','description'));
            } catch (Exception $e) {
              return redirect()->route('payments.booking',\Session::get('rbooking_id'))
                              ->withErrors($e->getMessage())
                              ->withInput();
              //return $e->getMessage();
            }

           //return $charge;
            
        }
    }

    public function getCustomerInvoice($id)
    {
      $invoice=Purchase::findOrFail($id);
      $user=User::findOrFail($invoice->user_id);
      return view('public.payments.customer_invoice',compact('invoice','user'));
    }

    public function showInvoiceDetails($id)
    {
      $invoice=Purchase::findOrFail($id);
      $user=User::findOrFail($invoice->user_id);
      return view('public.payments.user_invoice',compact('invoice','user'));
    }

    public function showBookingDetails($id)
    {
      $invoice=Purchase::findOrFail($id);
      $user=User::findOrFail($invoice->user_id);
      return view('public.invoice.booking',compact('invoice','user'));
    }

    public function getInvoices(){
      if(Auth::user()->role=='admin')
      {
        $invoices=Purchase::all();
      }
      else if(Auth::user()->role=='user')
      {
        $invoices=Purchase::where('user_id',Auth::user()->id)->paginate('10');
      }
      return view('public.invoice.index',compact('invoices'));
    }

    public function maketransfers($id)
    {
          \Stripe\Stripe::setApiKey('sk_test_4yKye7EUOXIJnzphQUdNaiHl');
        // \Stripe\Stripe::setApiKey("sk_test_fOipzEQI5NXQGpBDNBrKbI0B");
          $invoice=Purchase::findOrFail($id);
          if ($invoice->status!=1) {
            return redirect()->route('home.index');
          }
          $customer_id=$invoice->customer_id;

          $user=User::findOrFail(Auth::user()->id);
          $guide_name=$user->name;

          $experience_id=$invoice->experience_id;
          $experience=Experience::findOrFail($experience_id);
          $description=$experience->title;

          // $stripe_customer=Customer::where('user_id',$customer_id)->first();
          // $stripe_customer_id=$stripe_customer->customer_id;
          $stripe_customer_id=$invoice->stripe_customer_id;

          $destination_id=UserStripe::where('user_id',Auth::user()->id)->value('stripe_user_id');
          $application_fee=ceil(($invoice->vat+$invoice->fee+$invoice->stripe_fees)*100);
          $people=$invoice->people;
          $children=$invoice->children;

          $date=date_create($invoice->scheduled_date);
          $new_date=date_format($date,"d. F Y");
          $scheduled_time=$invoice->scheduled_time;

          $charge = \Stripe\Charge::create(array(
          'customer'    => $stripe_customer_id,
          'amount'      => ceil($invoice->amount*100),
          'currency'    => 'eur',
          'application_fee' =>$application_fee, // amount in cents
          'destination' => $destination_id
          ));

        $customer_name=$invoice->firstname_booking." ".$invoice->lastname_booking;
        $email_data = array(
        'experience_id' => $invoice->experience_id ,
        'name'  => $customer_name ,
        'guides_name'  => $guide_name,
        'description'  => $description,
        'scheduled_date'  => $new_date,
        'scheduled_time'  => $scheduled_time,
        'people'  => $people,
        'children'  => $children, 
        'amount' => $invoice->amount,
        );
        Mail::send('public.payments.booking_confirmation', $email_data, function ($message) use ($invoice) {
        $message->to($invoice->email_booking,$invoice->firstname_booking)->subject('Deine Buchung wurde bestätigt');
          });
          $invoice->stripe_transaction_id=$charge->id;
          $invoice->status='2';
          $invoice->save();

        /* ==========================================================================
           Initiate conversation between customer and tour guide
           ========================================================================== */
        $user_one=Auth::user()->id;
        $user_two=$customer_id;
        $reply=trans('booking.message_msg4');
        // check if the conversation exists
        $sq=DB::select( DB::raw("SELECT c_id FROM conversation WHERE (user_one='$user_one' and user_two='$user_two') 
        or (user_one='$user_two' and user_two='$user_one')") );
        $time=time();
        $ip=$_SERVER['REMOTE_ADDR'];
        $conversation_reply=ConversationReply::all();
        $user_conversation= new UserConversation();
        // if the conversation does not exist, create a new one
        if (sizeof($sq)==0) {
        $conversation_id=DB::table('conversation')->insertGetId([
            'user_one'=>$user_one,
            'user_two'=>$user_two,
            'ip'=>$ip,
            'time'=>$time,
        ]); 
         $c_id=$conversation_id;
         $user_conversation=UserConversation::create([
            'user_id'=>$user_one,
            'c_id'=>$c_id,
            'read_status'=>0,
            ]);
        $user_conversation2=UserConversation::create([
            'user_id'=>$user_two,
            'c_id'=>$c_id,
            'read_status'=>1,
            ]);
        }
        else
        {
            // else update the conversation details
            $c_id=$sq[0]->c_id;
        DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_one)
            ->update(['read_status' => 0]);
         DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_two)
            ->update(['read_status' => 1]);
        $date_time=DATE('Y-m-d H:i:s');
        DB::table('conversation')
            ->where('c_id', $c_id)
            ->update(['ts' => $date_time]);
        }
        // insert the new message
        $user_conversation2=ConversationReply::create([
            'user_id_fk'=>$user_one,
            'user_id_fk_2'=>$user_two,
            'reply'=>$reply,
            'ip'=>$ip,
            'time'=>$time,
            'c_id_fk'=>$c_id,
            ]);
           // end of conversation initiation
          \Session::flash('booking_success','Booking request successfully accepted.');
          return redirect()->route('home.default');
    }

    public function makeRefund($id)
    {
        $invoice=Purchase::findOrFail($id);
        if ($invoice->status!=1) {
            return redirect()->route('home.index');
        }
        $invoice->status='3';
        $invoice->save();
        $customer_name=$invoice->firstname_booking." ".$invoice->lastname_booking;
        $email_data = array(
        'experience_id' => $invoice->experience_id ,
        'name'  => $customer_name ,
        );
        Mail::send('public.payments.booking_rejection', $email_data, function ($message) use ($invoice) {
        $message->to($invoice->email_booking,$invoice->firstname_booking)->subject('Buchung wurde abgelehnt');
          });

          /* ==========================================================================
           Initiate conversation between tour guide and user
           ========================================================================== */
        $user_one=$invoice->user_id;
        $user_two=$invoice->customer_id;

        $reply=trans('booking.message_msg5');
        // check if the conversation exists
        $sq=DB::select( DB::raw("SELECT c_id FROM conversation WHERE (user_one='$user_one' and user_two='$user_two') 
        or (user_one='$user_two' and user_two='$user_one')") );
        $time=time();
        $ip=$_SERVER['REMOTE_ADDR'];
        $conversation_reply=ConversationReply::all();
        $user_conversation= new UserConversation();
        // if the conversation does not exist, create a new one
        if (sizeof($sq)==0) {
        $conversation_id=DB::table('conversation')->insertGetId([
            'user_one'=>$user_one,
            'user_two'=>$user_two,
            'ip'=>$ip,
            'time'=>$time,
        ]); 
         $c_id=$conversation_id;
         $user_conversation=UserConversation::create([
            'user_id'=>$user_one,
            'c_id'=>$c_id,
            'read_status'=>0,
            ]);
        $user_conversation2=UserConversation::create([
            'user_id'=>$user_two,
            'c_id'=>$c_id,
            'read_status'=>1,
            ]);
        }
        else
        {
            // else update the conversation details
            $c_id=$sq[0]->c_id;
        DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_one)
            ->update(['read_status' => 0]);
         DB::table('user_conversations')
            ->where('c_id', $c_id)
            ->where('user_id',$user_two)
            ->update(['read_status' => 1]);
        $date_time=DATE('Y-m-d H:i:s');
        DB::table('conversation')
            ->where('c_id', $c_id)
            ->update(['ts' => $date_time]);
        }
        // insert the new message
        $user_conversation2=ConversationReply::create([
            'user_id_fk'=>$user_one,
            'user_id_fk_2'=>$user_two,
            'reply'=>$reply,
            'ip'=>$ip,
            'time'=>$time,
            'c_id_fk'=>$c_id,
            ]);
        /* ==========================================================================
           End of conversation between tour guide and user
           ========================================================================== */

        \Session::flash('refund_success','Booking request successfully declined.');
        return redirect()->route('home.default');
    }
}
