<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Http\Requests;
use App\Pricing;
use App\Urgency;

class PricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::select('id','name')->get();
       return view('pricing',compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function showpricing(Request $request){
        $input_data = $request->all();
        $spacing=$input_data['spacing'];
        $pages=$input_data['pages'];
        $product_id=$input_data['product_id'];
        $product=Product::findOrFail($product_id);
        $base_price=$product->price;
        // echo $base_price;
        $pricing=Pricing::where('urgency','>=',0)->where('product_id','>=',$product_id)->get();
        $spacing=Pricing::where('spacing','>=',$spacing)->where('product_id','>=',$product_id)->get();
        $quality=Pricing::where('quality','>=',1)->where('product_id','>=',$product_id)->get();
        $quality2=Pricing::where('quality','>=',2)->where('product_id','>=',$product_id)->get();
        $spacing=$spacing[0]['attributes']['price'];
        $quality=$quality[0]['attributes']['price'];
        $quality2=$quality2[0]['attributes']['price'];

        $urgency=Urgency::get();
                // dd(count($urgency));
        // echo count($pricing);
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>Urgency</th>";
        echo "<th>Standard Quality</th>";
        echo "<th>Premium Quality</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        $count=0;
        foreach ($urgency as $value) {
            $total=$base_price+$spacing+$quality+$pricing[$count]['attributes']['price'];
            $total2=$base_price+$spacing+$quality2+$pricing[$count]['attributes']['price'];
           echo "<tr>";
           echo"<td>".$value->name."</td>";
           echo"<td>".$total."</td>";
           echo "<td>".$total2."</td>";
           echo "</tr>";
           $count++;
        }
        echo "</tbody>";
        echo "</table>";
    }
}
