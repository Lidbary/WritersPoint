<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Session;
use App\Order;
use Redirect;
use Validator;
use Auth;
use DB;
use App\User;
use App\File;
use App\Country;
use App\Product;
use App\Urgency;
use App\Pricing;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $products = DB::table('products')->get();
        return view('products.manage_product', ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $urgency=Urgency::all();
        return view('products.create_product',compact('urgency'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input_data = $request->all();
        $product=New Product;
        $product->name=$input_data['name'];
        $product->price=$input_data['baseprice'];
        $product->description=$input_data['description'];
        $product->save();

    foreach ($input_data as $key => $value) {
            $exploded=explode('_', $key);
            if($exploded[0]=='level'){
                    $pricing=New Pricing;
                    $pricing->academic_level=$exploded[1];
                    $pricing->price=$value;
                    $pricing->product_id=$product->id;
                    $pricing->save();
            }elseif($exploded[0]=='quality'){
                    $pricing=New Pricing;
                    $pricing->quality=$exploded[1];
                    $pricing->price=$value;
                    $pricing->product_id=$product->id;
                    $pricing->save();
            }elseif($exploded[0]=='spacing'){
                    $pricing=New Pricing;
                    $pricing->spacing=$exploded[1];
                    $pricing->price=$value;
                    $pricing->product_id=$product->id;
                    $pricing->save();
            }elseif($exploded[0]=='urgency'){
                    $pricing=New Pricing;
                    $pricing->urgency=$exploded[1];
                    $pricing->price=$value;
                    $pricing->product_id=$product->id;
                    $pricing->save();
            }
        }
        Session::flash('success_message', 'Product created successifuly!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product=Product::findOrFail($id);
        return view("products.show",compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
