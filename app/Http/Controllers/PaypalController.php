<?php

namespace App\Http\Controllers;
use App\includes\Adaptive;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App;
use Mail;
use DB;
use App\Order;

class PaypalController extends Controller
{
    // /**
    //  * Display a listing of the resource.
    //  *
    //  * @return \Illuminate\Http\Response
    //  */
    // public function LangDe(){
    //         $lag= 'de';
    //     App::setLocale("German");
    //     return redirect()->route('home.index');



    // }
    // public function LangEn(){
    //     App::setLocale('en');

    //     return redirect()->back();
    // }

    // public function makeRequest(Request $request,$id)
    // {
    //     $order=Order::findOrFail($id); 
    //     include(app_path().'/includes/includes/config.php');
    //     include(app_path().'/includes/vendor/autoload.php');
    //     // include(app_path().'/includes/angelleye/paypal-php-library/src/angelleye/PayPal/Adaptive');
        
    //     // Create PayPal object.
    //     $PayPalConfig = array(
    //         'Sandbox' => $sandbox,
    //         'DeveloperAccountEmail' => $developer_account_email,
    //         'ApplicationID' => $application_id,
    //         'DeviceID' => $device_id,
    //         'IPAddress' => $_SERVER['REMOTE_ADDR'],
    //         'APIUsername' => $api_username,
    //         'APIPassword' => $api_password,
    //         'APISignature' => $api_signature,
    //         'APISubject' => $api_subject,
    //         'PrintHeaders' => $print_headers,
    //         'LogResults' => $log_results,
    //         'LogPath' => $log_path,
    //     );

    //     $PayPal = new Adaptive($PayPalConfig);

    //     // Prepare request arrays
    //     $PayRequestFields = array(
    //         'ActionType' => 'PAY',                                // Required.  Whether the request pays the receiver or whether the request is set up to create a payment request, but not fulfill the payment until the ExecutePayment is called.  Values are:  PAY, CREATE, PAY_PRIMARY
    //         'CancelURL' => $domain . '/payments/cancel/'.$id,                                    // Required.  The URL to which the sender's browser is redirected if the sender cancels the approval for the payment after logging in to paypal.com.  1024 char max.
    //         'CurrencyCode' => 'USD',                                // Required.  3 character currency code.
    //         'FeesPayer' => 'EACHRECEIVER',                                    // The payer of the fees.  Values are:  SENDER, PRIMARYRECEIVER, EACHRECEIVER, SECONDARYONLY
    //         'IPNNotificationURL' => '',                        // The URL to which you want all IPN messages for this payment to be sent.  1024 char max.
    //         'Memo' => '',                                        // A note associated with the payment (text, not HTML).  1000 char max
    //         'Pin' => '',                                        // The sener's personal id number, which was specified when the sender signed up for the preapproval
    //         'PreapprovalKey' => '',                            // The key associated with a preapproval for this payment.  The preapproval is required if this is a preapproved payment.  
    //         'ReturnURL' => $domain . '/payments/return/'.$id,                                    // Required.  The URL to which the sener's browser is redirected after approvaing a payment on paypal.com.  1024 char max.
    //         'ReverseAllParallelPaymentsOnError' => '',            // Whether to reverse paralel payments if an error occurs with a payment.  Values are:  TRUE, FALSE
    //         'SenderEmail' => '',                                // Sender's email address.  127 char max.
    //         'TrackingID' => ''                                    // Unique ID that you specify to track the payment.  127 char max.
    //     );

    //     $ClientDetailsFields = array(
    //         'CustomerID' => '',                                // Your ID for the sender  127 char max.
    //         'CustomerType' => '',                                // Your ID of the type of customer.  127 char max.
    //         'GeoLocation' => '',                                // Sender's geographic location
    //         'Model' => '',                                        // A sub-identification of the application.  127 char max.
    //         'PartnerName' => ''                                    // Your organization's name or ID
    //     );

    //     $FundingTypes = array('ECHECK', 'BALANCE', 'CREDITCARD');                    // Funding constrainigs require advanced permissions levels.

    //     $Receivers = array();
    //     $Receiver = array(
    //         'Amount' => $order->client_price,                                            // Required.  Amount to be paid to the receiver.
    //         'Email' => 'liddypedia-facilitator@gmail.com',                                                // Receiver's email address. 127 char max.
    //         'InvoiceID' => $order->id,                                            // The invoice number for the payment.  127 char max.
    //         'PaymentType' => '',                                        // Transaction type.  Values are:  GOODS, SERVICE, PERSONAL, CASHADVANCE, DIGITALGOODS
    //         'PaymentSubType' => '',                                    // The transaction subtype for the payment.
    //         'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => ''), // Receiver's phone number.   Numbers only.
    //         'Primary' => 'false'                                                // Whether this receiver is the primary receiver.  Values are boolean:  TRUE, FALSE
    //     );
    //     array_push($Receivers, $Receiver);

    //     $SenderIdentifierFields = array(
    //         'UseCredentials' => ''                        // If TRUE, use credentials to identify the sender.  Default is false.
    //     );

    //     $AccountIdentifierFields = array(
    //         'Email' => '',                                // Sender's email address.  127 char max.
    //         'Phone' => array('CountryCode' => '', 'PhoneNumber' => '', 'Extension' => '')                                // Sender's phone number.  Numbers only.
    //     );

    //     $PayPalRequestData = array(
    //         'PayRequestFields' => $PayRequestFields,
    //         'ClientDetailsFields' => $ClientDetailsFields,
    //         //'FundingTypes' => $FundingTypes, 
    //         'Receivers' => $Receivers,
    //         'SenderIdentifierFields' => $SenderIdentifierFields,
    //         'AccountIdentifierFields' => $AccountIdentifierFields
    //     );


    //     // Pass data into class for processing with PayPal and load the response array into $PayPalResult
    //     $PayPalResult = $PayPal->Pay($PayPalRequestData);

    //     $paykey=$PayPalResult['PayKey'];
    //     $order->track_id=$paykey;
    //     $order->save();
    //     $redirectUrl=$PayPalResult['RedirectURL'];
    //     return redirect($redirectUrl);
    // }

    public function cancelRequest($id)
    {
        $order=Order::findOrFail($id);
        $order->paid=0;
        $order->save();
        $lastrecord=Order::orderBy('id', 'desc')->first();
        $new_id=$lastrecord->id+1;
        $neworder=$order->replicate();
        $neworder->id=$new_id;
        $neworder->save();
        $order->delete();
        \Session::flash('error_message','The payment failed.Please try again');
        if (Auth::check()) {
            return redirect()->route('new_order');
        }else{
           return redirect()->route('preview_order',$neworder->id); 
        }
        
    }

    public function returnRequest($id)
    {
        $order=Order::findOrFail($id);
        $order->paid=1;
        $order->save();
        if (Auth::check()) {
            return redirect()->route('manage-orders');
        }else{
            $user_id=$order->user_id;
            $user=User::findOrFail($user_id);
            Auth::login($user);
            return redirect('home');            
        }

      //   include(app_path().'/includes/includes/config.php');
      //   include(app_path().'/includes/vendor/autoload.php');
      //         // Create PayPal object.
      //   $PayPalConfig = array(
      //       'Sandbox' => $sandbox,
      //       'DeveloperAccountEmail' => $developer_account_email,
      //       'ApplicationID' => $application_id,
      //       'DeviceID' => $device_id,
      //       'IPAddress' => $_SERVER['REMOTE_ADDR'],
      //       'APIUsername' => $api_username,
      //       'APIPassword' => $api_password,
      //       'APISignature' => $api_signature,
      //       'APISubject' => $api_subject,
      //       'PrintHeaders' => $print_headers,
      //       'LogResults' => $log_results,
      //       'LogPath' => $log_path,
      //   );

      //   $PayPal = new Adaptive($PayPalConfig);

      //   // Prepare request arrays
      //   $PaymentDetailsFields = array(
      //       'PayKey' => $purchase->paypal_key,                            // The pay key that identifies the payment for which you want to retrieve details.
      //       'TransactionID' => '',                        // The PayPal transaction ID associated with the payment.
      //       'TrackingID' => ''                            // The tracking ID that was specified for this payment in the PayRequest message.  127 char max.
      //   );

      //   $PayPalRequestData = array('PaymentDetailsFields' => $PaymentDetailsFields);


      //   // Pass data into class for processing with PayPal and load the response array into $PayPalResult
      //   $PayPalResult = $PayPal->PaymentDetails($PayPalRequestData);
      //   $transaction_id=$PayPalResult['PaymentInfo']['TransactionID'];
      //   $purchase->paypal_transaction_id=$transaction_id;
      //   $purchase->save();
      // return view('public.payments.confirmation',compact('order_id','quantity','brand','customer_name','description'));
    }

    // public function maketransfers($id)
    // {
    //       $invoice=Purchase::findOrFail($id);
    //      if ($invoice->status!=1) {
    //         return redirect()->route('home.index');
    //       }
    //       $customer_id=$invoice->customer_id;
    //       $experience_id=$invoice->experience_id;

    //       $user=User::findOrFail(Auth::user()->id);
    //       $guide_name=$user->name;

    //       $experience=Experience::findOrFail($experience_id);
    //       $description=$experience->title;

    //       $paypal_key=$invoice->paypal_key;

    //       $application_fee=ceil(($invoice->vat+$invoice->fee+$invoice->stripe_fees)*100);
    //       $people=$invoice->people;
    //       $children=$invoice->children;

    //       $date=date_create($invoice->scheduled_date);
    //       $new_date=date_format($date,"d. F Y");
    //       $scheduled_time=$invoice->scheduled_time;

    //     // pay guide
    //     include(app_path().'/includes/includes/config.php');
    //     include(app_path().'/includes/vendor/autoload.php');

    //       // Create PayPal object.
    //     $PayPalConfig = array(
    //         'Sandbox' => $sandbox,
    //         'DeveloperAccountEmail' => $developer_account_email,
    //         'ApplicationID' => $application_id,
    //         'DeviceID' => $device_id,
    //         'IPAddress' => $_SERVER['REMOTE_ADDR'],
    //         'APIUsername' => $api_username,
    //         'APIPassword' => $api_password,
    //         'APISignature' => $api_signature,
    //         'APISubject' => $api_subject,
    //         'PrintHeaders' => $print_headers,
    //         'LogResults' => $log_results,
    //         'LogPath' => $log_path,
    //     );

    //     $PayPal = new Adaptive($PayPalConfig);

    //     $customer_name=$invoice->firstname_booking." ".$invoice->lastname_booking;
    //     $email_data = array(
    //     'experience_id' => $invoice->experience_id ,
    //     'name'  => $customer_name ,
    //     'guides_name'  => $guide_name,
    //     'description'  => $description,
    //     'scheduled_date'  => $new_date,
    //     'scheduled_time'  => $scheduled_time,
    //     'people'  => $people,
    //     'children'  => $children, 
    //     'amount' => $invoice->amount,
    //     );

    //     // Prepare request arrays
    //     $ExecutePaymentFields = array(
    //         'PayKey' => $paypal_key,                                 // The pay key that identifies the payment to be executed.  This is the key returned in the PayResponse message.
    //         'FundingPlanID' => ''                           // The ID of the funding plan from which to make this payment.
    //     );

    //     $PayPalRequestData = array('ExecutePaymentFields' => $ExecutePaymentFields);

    //     // Pass data into class for processing with PayPal and load the response array into $PayPalResult
    //     $PayPalResult = $PayPal->ExecutePayment($PayPalRequestData);

    //     // Write the contents of the response array to the screen for demo purposes.
    //     echo '<pre />';
       

    //     Mail::send('public.payments.booking_confirmation', $email_data, function ($message) use ($invoice) {
    //     $message->to($invoice->email_booking,$invoice->firstname_booking)->subject('Deine Buchung wurde bestätigt');
    //       });
    //       $invoice->status='2';
    //       $invoice->save();

    //     // dd($PayPalResult);

    //     /* ==========================================================================
    //        Initiate conversation between customer and tour guide
    //        ========================================================================== */
    //     $user_one=Auth::user()->id;
    //     $user_two=$customer_id;
    //     $reply=trans('booking.message_msg4');
    //     // check if the conversation exists
    //     $sq=DB::select( DB::raw("SELECT c_id FROM conversation WHERE (user_one='$user_one' and user_two='$user_two') 
    //     or (user_one='$user_two' and user_two='$user_one')") );
    //     $time=time();
    //     $ip=$_SERVER['REMOTE_ADDR'];
    //     $conversation_reply=ConversationReply::all();
    //     $user_conversation= new UserConversation();
    //     // if the conversation does not exist, create a new one
    //     if (sizeof($sq)==0) {
    //     $conversation_id=DB::table('conversation')->insertGetId([
    //         'user_one'=>$user_one,
    //         'user_two'=>$user_two,
    //         'ip'=>$ip,
    //         'time'=>$time,
    //     ]); 
    //      $c_id=$conversation_id;
    //      $user_conversation=UserConversation::create([
    //         'user_id'=>$user_one,
    //         'c_id'=>$c_id,
    //         'read_status'=>0,
    //         ]);
    //     $user_conversation2=UserConversation::create([
    //         'user_id'=>$user_two,
    //         'c_id'=>$c_id,
    //         'read_status'=>1,
    //         ]);
    //     }
    //     else
    //     {
    //         // else update the conversation details
    //         $c_id=$sq[0]->c_id;
    //     DB::table('user_conversations')
    //         ->where('c_id', $c_id)
    //         ->where('user_id',$user_one)
    //         ->update(['read_status' => 0]);
    //      DB::table('user_conversations')
    //         ->where('c_id', $c_id)
    //         ->where('user_id',$user_two)
    //         ->update(['read_status' => 1]);
    //     $date_time=DATE('Y-m-d H:i:s');
    //     DB::table('conversation')
    //         ->where('c_id', $c_id)
    //         ->update(['ts' => $date_time]);
    //     }
    //     // insert the new message
    //     $user_conversation2=ConversationReply::create([
    //         'user_id_fk'=>$user_one,
    //         'user_id_fk_2'=>$user_two,
    //         'reply'=>$reply,
    //         'ip'=>$ip,
    //         'time'=>$time,
    //         'c_id_fk'=>$c_id,
    //         ]);
    //        // end of conversation initiation
    //       \Session::flash('booking_success','Booking request successfully accepted.');
    //       return redirect()->route('home.default');
    // }

    // public function makeRefund($id)
    // {
    //     $invoice=Purchase::findOrFail($id);
    //     if ($invoice->status!=1) {
    //         return redirect()->route('home.index');
    //     }
    //     $invoice->status='3';
    //     $invoice->save();
    //     $customer_name=$invoice->firstname_booking." ".$invoice->lastname_booking;
    //     $paypal_key=$invoice->paypal_key;
    //     $amount=$invoice->amount * 0.95;
    //     $email_data = array(
    //     'experience_id' => $invoice->experience_id ,
    //     'name'  => $customer_name ,
    //     );

    //     //refund payment
    //             // Include required library files.
    //     include(app_path().'/includes/includes/config.php');
    //     include(app_path().'/includes/vendor/autoload.php');



    //     // Create PayPal object.
    //     $PayPalConfig = array(
    //         'Sandbox' => $sandbox,
    //         'DeveloperAccountEmail' => $developer_account_email,
    //         'ApplicationID' => $application_id,
    //         'DeviceID' => $device_id,
    //         'IPAddress' => $_SERVER['REMOTE_ADDR'],
    //         'APIUsername' => $api_username,
    //         'APIPassword' => $api_password,
    //         'APISignature' => $api_signature,
    //         'APISubject' => $api_subject,
    //         'PrintHeaders' => $print_headers,
    //         'LogResults' => $log_results,
    //         'LogPath' => $log_path,
    //     );

    //     $PayPal = new Adaptive($PayPalConfig);

    //     // Prepare request arrays
    //     $RefundFields = array(
    //         'CurrencyCode' => 'USD',                                            // Required.  Must specify code used for original payment.  You do not need to specify if you use a payKey to refund a completed transaction.
    //         'PayKey' => $paypal_key,                                                    // Required.  The key used to create the payment that you want to refund.
    //         'TransactionID' => '',                            // Required.  The PayPal transaction ID associated with the payment that you want to refund.
    //         'TrackingID' => ''                                                // Required.  The tracking ID associated with the payment that you want to refund.
    //     );

    //     $Receivers = array();
    //     $Receiver = array(
    //         'Email' => 'liddypedia-facilitator@gmail.com',            // A receiver's email address. 
    //         'Amount' => $amount,                                    // Amount to be debited to the receiver's account.
    //         'Primary' => '',                                            // Set to true to indicate a chained payment.  Only one receiver can be a primary receiver.  Omit this field, or set to false for simple and parallel payments.
    //         'InvoiceID' => '',                                        // The invoice number for the payment.  This field is only used in Pay API operation.
    //         'PaymentType' => ''                                    // The transaction subtype for the payment.  Allowable values are: GOODS, SERVICE
    //     );

    //     array_push($Receivers, $Receiver);

    //     $PayPalRequestData = array(
    //         'RefundFields' => $RefundFields,
    //         'Receivers' => $Receivers
    //     );


    //     // Pass data into class for processing with PayPal and load the response array into $PayPalResult
    //     $PayPalResult = $PayPal->Refund($PayPalRequestData);

    //     // Write the contents of the response array to the screen for demo purposes.
    //     // echo '<pre />';
    //     // dd($PayPalResult);
    //     Mail::send('public.payments.booking_rejection', $email_data, function ($message) use ($invoice) {
    //     $message->to($invoice->email_booking,$invoice->firstname_booking)->subject('Buchung wurde abgelehnt');
    //       });
    //      /* ==========================================================================
    //        Initiate conversation between tour guide and user
    //        ========================================================================== */
    //     $user_one=$invoice->user_id;
    //     $user_two=$invoice->customer_id;

    //     $reply=trans('booking.message_msg5');
    //     // check if the conversation exists
    //     $sq=DB::select( DB::raw("SELECT c_id FROM conversation WHERE (user_one='$user_one' and user_two='$user_two') 
    //     or (user_one='$user_two' and user_two='$user_one')") );
    //     $time=time();
    //     $ip=$_SERVER['REMOTE_ADDR'];
    //     $conversation_reply=ConversationReply::all();
    //     $user_conversation= new UserConversation();
    //     // if the conversation does not exist, create a new one
    //     if (sizeof($sq)==0) {
    //     $conversation_id=DB::table('conversation')->insertGetId([
    //         'user_one'=>$user_one,
    //         'user_two'=>$user_two,
    //         'ip'=>$ip,
    //         'time'=>$time,
    //     ]); 
    //      $c_id=$conversation_id;
    //      $user_conversation=UserConversation::create([
    //         'user_id'=>$user_one,
    //         'c_id'=>$c_id,
    //         'read_status'=>0,
    //         ]);
    //     $user_conversation2=UserConversation::create([
    //         'user_id'=>$user_two,
    //         'c_id'=>$c_id,
    //         'read_status'=>1,
    //         ]);
    //     }
    //     else
    //     {
    //         // else update the conversation details
    //         $c_id=$sq[0]->c_id;
    //     DB::table('user_conversations')
    //         ->where('c_id', $c_id)
    //         ->where('user_id',$user_one)
    //         ->update(['read_status' => 0]);
    //      DB::table('user_conversations')
    //         ->where('c_id', $c_id)
    //         ->where('user_id',$user_two)
    //         ->update(['read_status' => 1]);
    //     $date_time=DATE('Y-m-d H:i:s');
    //     DB::table('conversation')
    //         ->where('c_id', $c_id)
    //         ->update(['ts' => $date_time]);
    //     }
    //     // insert the new message
    //     $user_conversation2=ConversationReply::create([
    //         'user_id_fk'=>$user_one,
    //         'user_id_fk_2'=>$user_two,
    //         'reply'=>$reply,
    //         'ip'=>$ip,
    //         'time'=>$time,
    //         'c_id_fk'=>$c_id,
    //         ]);
    //     /* ==========================================================================
    //        End of conversation between tour guide and user
    //        ========================================================================== */
    //     \Session::flash('refund_success','Booking request successfully declined.');
    //     return redirect()->route('home.default');
    // }

}
