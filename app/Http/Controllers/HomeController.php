<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use App\Order;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->only('index');;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $orders = DB::table('orders')->select('orders.id AS ordersid', 'user_id', 'firstname', 'orders.citation', 'orders.created_at', 'doctype', 'order_title', 'order_level', 'max_bid', 'spacing', 'digital_sources', 'discipline', 'deadline', 'client_price', 'track_id', 'no_of_sources', 'instructions', 'no_of_pages')
        ->join('users', 'users.id', '=', 'orders.user_id')->get();
        $available = Order::where('available', 1)->get();
        return view('home', ['orders'=>$orders, 'available'=>$available]);
    }
    public function about(){
        return view('about');
    }
    public function contactus(){
        return view('contactus');
    }
}
