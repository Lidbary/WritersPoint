<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Message;
use App\User;
use Session;
use Redirect;
use Validator;
use Auth;
use DB;

class MessagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        $messages = DB::table('messages')
        ->join('users','users.id', '=', 'messages.user_id')
        ->where('to_id', Auth::user()->id)->get();
        return view('mails.messages', ['messages'=>$messages, 'users'=>$users]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
       return view('mails.messages', []);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'message'       => 'required',
           
           
        );
        $validator = Validator::make($input_data = $request->all(), $rules);

        // process form
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator);
        } else {
  

        $newmessage = New Message;
        $newmessage->user_id = Auth::user()->id;
        $newmessage->subject = $input_data['subject'];
        $file = $input_data['message_file'];

        $destinationPath = 'uploads/message_files'; // upload path
        $extension = $file->getClientOriginalExtension();
        $fileName = rand(1111111111, 9999999999) . '.' . $extension;

        $file->move($destinationPath, $fileName); // uploading file to given path
        $newmessage->message_file = $fileName;
        $newmessage->from_id = Auth::user()->id;
        $newmessage->to_id = $input_data['user_id'];
        $newmessage->message = $input_data['message'];

        $newmessage->save();

        Session::flash('success_message', 'message sent successifuly!');
        return redirect()->route('messages');
    }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
