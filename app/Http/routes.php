<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::get('/home', 'HomeController@index');
Route::get('/about', 'HomeController@about');
Route::get('/contactus', 'HomeController@contactus');
Route::get('/pricing', 'PricingController@index');
Route::get('/order', 'OrdersController@order');
Route::post('/order', 'OrdersController@postorder');
Route::get('/checkout', 'OrdersController@checkout');
Route::post('/orderdata', 'OrdersController@orderdata');
Route::get('/orderdata', 'OrdersController@orderdata');
// Route::post('/showpricing', 'PricingController@showpricing');
Route::get('/showpricing', 'PricingController@showpricing');
Route::get('manage-orders', ['uses'=> 'OrdersController@manage', 'as' => 'manage-orders']);
Route::get('new_order', ['uses'=> 'OrdersController@create', 'as' => 'new_order']);
Route::post('create_order', ['uses'=> 'OrdersController@store', 'as' => 'create_order']);
Route::post('upload_file', ['uses'=> 'OrdersController@upload', 'as' => 'upload_file']);
Route::post('assign_order', ['uses'=> 'OrdersController@assignorder', 'as' => 'assign_order']);
Route::get('create_user', ['uses'=> 'UsersController@create', 'as' => 'create_user', 'middleware'=>'roles', 'roles'=>['admin']]);
Route::post('create_user', ['uses'=> 'UsersController@store', 'as' => 'create_user']);
Route::get('{id}/view_order', ['uses'=> 'OrdersController@show', 'as' => 'view_order']);
Route::get('{id}/preview_another',['as' => 'preview_another' ,'uses' =>'OrdersController@previewAnother']);
Route::get('messages', ['uses'=> 'MessagesController@index', 'as' => 'messages']);
Route::post('new_message', ['uses'=> 'MessagesController@store', 'as' => 'new_message']);
Route::get('site_settings', ['uses'=> 'SettingsController@create', 'as' => 'site_settings']);
Route::get('payments', ['uses'=> 'PaymentsController@create', 'as' => 'payments']);
Route::get('bids', ['uses'=> 'BidsController@create', 'as' => 'bids']);
Route::get('users', ['uses'=> 'UsersController@index', 'as' => 'users']);
Route::get('products', ['uses'=> 'ProductsController@index', 'as' => 'products']);
Route::get('new_product', ['uses'=> 'ProductsController@create', 'as' => 'new_product']);
Route::post('new_product', ['uses'=> 'ProductsController@store', 'as' => 'new_product']);


Route::group(['prefix' => 'order','middleware' => 'guest'], function () {
	Route::get('{id}/preview', ['uses'=> 'OrdersController@previeworder', 'as' => 'preview_order']);	
});
Route::group(['prefix' => 'order','middleware' => 'auth'], function () {
	Route::get('{id}/view', ['uses'=> 'OrdersController@show', 'as' => 'vieworder']);	
});


Route::group(['prefix' => 'product','middleware' => 'auth'], function () {
	Route::get('{id}/view', ['uses'=> 'ProductsController@show', 'as' => 'viewProduct']);	
});
//payment routes
Route::group(['prefix' => 'payments'], function () {
Route::post('pay/{id}',['as' => 'payOrder' ,'uses' =>'PaypalController@makeRequest']);
Route::get('pay/{id}',['as' => 'payOrderGet' ,'uses' =>'PaypalController@makeRequest']);
Route::get('cancel/{id}',['as' => 'payments.paypal_cancel' ,'uses' =>'PaypalController@cancelRequest']);
Route::get('return/{id}',['as' => 'payments.paypal_return' ,'uses' =>'PaypalController@returnRequest']);
Route::get('makepay/{id}',['as' => 'paypal.makepayments' ,'uses' => 'PaypalController@maketransfers']);
Route::get('refund/{id}',['as' => 'payments.refunding' ,'uses' => 'PaypalController@makeRefund']);
});


