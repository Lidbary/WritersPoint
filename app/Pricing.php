<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing extends Model
{
   protected $table="pricing";

   protected $fillable=['academic_level','urgency','spacing','quality','product_id','created_at','updated_at','price'];
}
