<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
   protected $fillable = ['message_file', 'to_id', 'from_id', 'subject', 'message'];
}