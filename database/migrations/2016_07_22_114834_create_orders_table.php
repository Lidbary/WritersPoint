<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('doctype');
            $table->string('order_title');
            $table->string('order_level');
            $table->string('max_bid');
            $table->string('no_of_pages');
            $table->string('spacing');
            $table->string('digital_sources');
            $table->string('discipline');
            $table->string('deadline');
            $table->string('client_price');
            $table->string('track_id');
            $table->string('citation');
            $table->string('no_of_sources');
            $table->string('instructions');
            $table->string('approved')->default('unapproved');
            $table->string('paid')->default('not paid');
            $table->string('banned')->default('not banned');
            $table->string('available')->default('not available');
            $table->string('assigned')->default('not assigned');
            $table->string('removed_order')->default('not removed');
            $table->string('fined')->default('not fined');
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
